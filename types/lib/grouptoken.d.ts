export = GroupToken;
/**
 * A GroupToken is merely a util containing methods for group tokenization for nexa blockchain.
 * @constructor
 */
declare function GroupToken(): void;
declare class GroupToken {
}
declare namespace GroupToken {
    namespace idFlags {
        let NONE: number;
        let COVENANT: number;
        let HOLDS_NEX: number;
        let GROUP_RESERVED_BITS: number;
        let DEFAULT: number;
    }
    namespace authFlags {
        export let AUTHORITY: bigint;
        export let MINT: bigint;
        export let MELT: bigint;
        export let BATON: bigint;
        export let RESCRIPT: bigint;
        export let SUBGROUP: bigint;
        let NONE_1: bigint;
        export { NONE_1 as NONE };
        export let ACTIVE_FLAG_BITS: bigint;
        export let ALL_FLAG_BITS: bigint;
        export let RESERVED_FLAG_BITS: bigint;
    }
    /**
     * Build OP_RETURN output script for Token Description
     *
     * @param {string} ticker
     * @param {string} name
     * @param {string} docUrl
     * @param {string} docHash
     * @param {number} decimals
     * @returns the output OP_RETURN script
     */
    function buildTokenDescScript(ticker: string, name: string, docUrl: string, docHash: string, decimals: number): import("./script/script");
    /**
     * Calculate a group ID based on the provided inputs. Pass 'null' to opReturnScript if there is not
     * going to be an OP_RETURN output in the transaction.
     *
     * @param {Buffer} outpointBuffer - The input outpoint hash buffer
     * @param {Buffer|null} opReturnScript - opReturn output script
     * @param {bigint} authFlag - group control flags
     * @param {number} idFlag - group id flags
     * @returns Object with group id hash buffer and the nonce bigint
     */
    function findGroupId(outpointBuffer: Buffer, opReturnScript: Buffer, authFlag: bigint, idFlag?: number): {
        hashBuffer: Buffer;
        nonce: bigint;
    };
    /**
     * Translates a group and additional data into a subgroup identifier
     *
     * @param {Address|string} group - the group/token address
     * @param {number|string} data - additional data
     * @returns the subgroup hashBuffer
     */
    function generateSubgroupId(group: string | Address, data: string | number): Buffer;
    /**
     * Get group amount buffer from BigInt to include in output script
     *
     * @param {bigint} amount
     * @returns {Buffer}
     */
    function getAmountBuffer(amount: bigint): Buffer;
    /**
     * @param {bigint} authFlag the output group quantity/authority
     * @returns {bigint} the nonce
     */
    function getNonce(authFlag: bigint): bigint;
    /**
     * @param {Buffer} groupId the group id buffer
     * @param {number} groupIdFlag the group id flag
     * @returns {boolean} true if this group id has the flag
     */
    function hasIdFlag(groupId: Buffer, groupIdFlag: number): boolean;
    /**
     * @param {Buffer} groupId the group id buffer
     * @param {bigint} authFlag the output group quantity/authority
     * @param {bigint} groupIdFlag the group id flag
     * @returns {boolean} true if this is group creation data
     */
    function isGroupCreation(groupId: Buffer, authFlag: bigint, groupIdFlag?: bigint): boolean;
    /**
     * @param {Buffer} groupId the group id buffer
     * @returns {boolean} true if this group id is subgroup
     */
    function isSubgroup(groupId: Buffer): boolean;
    /**
     * @param {bigint} authFlag the output group quantity/authority
     * @returns {boolean} true if this is authority flag
     */
    function isAuthority(authFlag: bigint): boolean;
    /**
     * @param {bigint} authFlag the output group quantity/authority
     * @returns {boolean} true if this flag allows minting.
     */
    function allowsMint(authFlag: bigint): boolean;
    /**
     * @param {bigint} authFlag the output group quantity/authority
     * @returns {boolean} true if this flag allows melting.
     */
    function allowsMelt(authFlag: bigint): boolean;
    /**
     * @param {bigint} authFlag the output group quantity/authority
     * @returns {boolean} true if this flag allows child controllers.
     */
    function allowsRenew(authFlag: bigint): boolean;
    /**
     * @param {bigint} authFlag the output group quantity/authority
     * @returns {boolean} true if this flag allows rescripting.
     */
    function allowsRescript(authFlag: bigint): boolean;
    /**
     * @param {bigint} authFlag the output group quantity/authority
     * @returns {boolean} true if this flag allows subgroups
     */
    function allowsSubgroup(authFlag: bigint): boolean;
    /**
     * Verify token description document json signature
     *
     * @param {string} jsonDoc the json TDD as string
     * @param {Address|string} address nexa address that signed the doc
     * @param {string} signature the signature. optional - if empty, extract from jsonDoc
     * @returns {boolean}
     */
    function verifyJsonDoc(jsonDoc: string, address: string | Address, signature: string): boolean;
    /**
     * Sign token description document json
     *
     * @param {string} jsonDoc the json TDD as string
     * @param {PrivateKey} privKey private key to sign on the doc
     * @returns {string}
     */
    function signJsonDoc(jsonDoc: string, privKey: PrivateKey): string;
}
import Address = require("./address");
//# sourceMappingURL=grouptoken.d.ts.map