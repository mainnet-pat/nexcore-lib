export = Address;
/**
 * Instantiate an address from an address String or Buffer, a public key or script hash Buffer,
 * or an instance of {@link PublicKey} or {@link Script}.
 *
 * This is an immutable class, and if the first parameter provided to this constructor is an
 * `Address` instance, the same argument will be returned.
 *
 * An address has two key properties: `network` and `type`. The type is either
 * `Address.PayToPublicKeyHash` (value is the `'pubkeyhash'` string)
 * or `Address.PayToScriptHash` (the string `'scripthash'`). The network is an instance of {@link Network}.
 * You can quickly check whether an address is of a given kind by using the methods
 * `isPayToPublicKeyHash` and `isPayToScriptHash`
 *
 * @example
 * ```javascript
 * // validate that an input field is valid
 * var error = Address.getValidationError(input, 'testnet');
 * if (!error) {
 *   var address = Address(input, 'testnet');
 * } else {
 *   // invalid network or checksum (typo?)
 *   var message = error.messsage;
 * }
 *
 * // get an address from a public key
 * var address = Address(publicKey, 'testnet').toString();
 * ```
 *
 * @param {*} data - The encoded data in various formats
 * @param {Networks.Network} network - The network: 'livenet' or 'testnet'
 * @param {string=} type - The type of address: 'script' or 'pubkey'
 * @returns {Address} A new valid and frozen instance of an Address
 * @constructor
 */
declare function Address(data: any, network: Networks.Network, type?: string | undefined): Address;
declare class Address {
    /**
     * Instantiate an address from an address String or Buffer, a public key or script hash Buffer,
     * or an instance of {@link PublicKey} or {@link Script}.
     *
     * This is an immutable class, and if the first parameter provided to this constructor is an
     * `Address` instance, the same argument will be returned.
     *
     * An address has two key properties: `network` and `type`. The type is either
     * `Address.PayToPublicKeyHash` (value is the `'pubkeyhash'` string)
     * or `Address.PayToScriptHash` (the string `'scripthash'`). The network is an instance of {@link Network}.
     * You can quickly check whether an address is of a given kind by using the methods
     * `isPayToPublicKeyHash` and `isPayToScriptHash`
     *
     * @example
     * ```javascript
     * // validate that an input field is valid
     * var error = Address.getValidationError(input, 'testnet');
     * if (!error) {
     *   var address = Address(input, 'testnet');
     * } else {
     *   // invalid network or checksum (typo?)
     *   var message = error.messsage;
     * }
     *
     * // get an address from a public key
     * var address = Address(publicKey, 'testnet').toString();
     * ```
     *
     * @param {*} data - The encoded data in various formats
     * @param {Networks.Network} network - The network: 'livenet' or 'testnet'
     * @param {string=} type - The type of address: 'script' or 'pubkey'
     * @returns {Address} A new valid and frozen instance of an Address
     * @constructor
     */
    constructor(data: any, network: Networks.Network, type?: string | undefined);
    /**
     * Returns true if an address is of pay to public key hash type
     * @return boolean
     */
    isPayToPublicKeyHash(): boolean;
    /**
     * Returns true if an address is of pay to script hash type
     * @return boolean
     */
    isPayToScriptHash(): boolean;
    /**
     * Returns true if an address is of pay to script template type
     * @return boolean
     */
    isPayToScriptTemplate(): boolean;
    /**
     * Returns true if an address is of pay to grouped template type
     * @return boolean
     */
    isGroupedPayToPublicKeyTemplate(): boolean;
    /**
     * Will return a buffer representation of the address
     *
     * @returns {Buffer} Bitcoin address buffer
    
    Address.prototype.toBuffer = function() {
      var version = Buffer.from([this.network[this.type]]);
      var buf = Buffer.concat([version, this.hashBuffer]);
      return buf;
    };
     */
    /**
     * @returns {Object} A plain object with the address information
     */
    toObject: () => any;
    toJSON(): any;
    /**
     * Will return a string formatted for the console
     *
     * @returns {string} Bitcoin address
     */
    inspect(): string;
    /**
     * Will return a cashaddr representation of the address. Always return lower case
     * Can be converted by the caller to uppercase is needed (still valid).
     *
     * @returns {string} Nexa address
     */
    toNexaAddress: () => string;
    toString(): string;
}
declare namespace Address {
    let PayToPublicKeyHash: string;
    let PayToScriptHash: string;
    let PayToScriptTemplate: string;
    let GroupedPayToPublicKeyTemplate: string;
    /**
     * @param {Buffer} hash - An instance of a hash Buffer
     * @returns {Object} An object with keys: hashBuffer
     * @private
     */
    function _transformHash(hash: Buffer): any;
    /**
     * @param {Buffer|Opcode|number} templateHash - An instance of a template hash Buffer
     * @param {Buffer|Opcode|number} constraintHash - An instance of a constraint hash Buffer
     * @param {Array} visibleArgs - An array of push-only args
     * @returns {Object} An object with keys: hashBuffer
     * @private
     */
    function _transformTemplate(templateHash: number | Buffer | Opcode, constraintHash: number | Buffer | Opcode, visibleArgs: any[]): any;
    /**
     * Deserializes an address serialized through `Address#toObject()`
     * @param {Object} data
     * @param {string} data.hash - the hash that this address encodes
     * @param {string} data.type - either 'pubkeyhash' or 'scripthash'
     * @param {Network=} data.network - the name of the network associated
     * @return {Address}
     */
    function _transformObject(data: {
        hash: string;
        type: string;
        network?: Network;
    }): Address;
    /**
     * Creates a P2SH address from a set of public keys and a threshold.
     *
     * The addresses will be sorted lexicographically, as that is the trend in bitcoin.
     * To create an address from unsorted public keys, use the {@link Script#buildMultisigOut}
     * interface.
     *
     * @param {Array} publicKeys - a set of public keys to create an address
     * @param {number} threshold - the number of signatures needed to release the funds
     * @param {String|Network} network - either a Network instance, 'livenet', or 'testnet'
     * @return {Address}
     */
    function createMultisig(publicKeys: any[], threshold: number, network: any): Address;
    /**
     * Creates a P2SH Zero-Confirmation Escrow (ZCE) address from a set of input public keys and a reclaim public key.
     *
     * @param {Array} inputPublicKeys - the set of public keys needed to sign all inputs in a ZCE transaction
     * @param {PublicKey} reclaimPublicKey - the public key required to reclaim the escrow
     * @param {String|Network} network - either a Network instance, 'livenet', or 'testnet'
     * @return {Address}
     */
    function createEscrow(inputPublicKeys: any[], reclaimPublicKey: PublicKey, network: any): Address;
    /**
     * Instantiate an address from a PublicKey instance
     *
     * @param {PublicKey} data
     * @param {String|Network} network - either a Network instance, 'livenet', or 'testnet'
     * @returns {Address} A new valid and frozen instance of an Address
     */
    function fromPublicKey(data: PublicKey, network: any, type: any): Address;
    /**
     * Internal function to transform a {@link PublicKey}
     *
     * @param {PublicKey} pubkey - An instance of PublicKey
     * @returns {Object} An object with keys: hashBuffer, type
     * @private
     */
    function _transformPublicKey(pubkey: PublicKey, type: any): any;
    /**
     * WARNING: This method is deprecated - P2SH disabled on nexa mainnet.
     * Instantiate an address from a ripemd160 script hash
     *
     * @deprecated
     * @param {Buffer} hash - An instance of buffer of the hash
     * @param {String|Network} network - either a Network instance, 'livenet', or 'testnet'
     * @returns {Address} A new valid and frozen instance of an Address
     */
    function fromScriptHash(hash: Buffer, network: any): Address;
    /**
     * Instantiate an address from a non grouped script template
     *
     * @param {Buffer|Opcode|number} templateHash - An instance the template hash
     * @param {Buffer|Opcode|number} constraintHash - An instance the constraint hash
     * @param {Array} visibleArgs - An array of push-only args
     * @param {Networks.Network} network - either a Network instance, 'livenet', or 'testnet'
     * @returns {Address} A new valid and frozen instance of an Address
     */
    function fromScriptTemplate(templateHash: number | Buffer | Opcode, constraintHash: number | Buffer | Opcode, visibleArgs?: any[], network?: Networks.Network): Address;
    /**
     * WARNING: This method is deprecated - P2SH disabled on nexa mainnet.
     *
     * Builds a p2sh address paying to script. This will hash the script and
     * use that to create the address.
     * If you want to extract an address associated with a script instead,
     * see {{Address#fromScript}}
     *
     * @deprecated
     * @param {Script} script - An instance of Script
     * @param {String|Network} network - either a Network instance, 'livenet', or 'testnet'
     * @returns {Address} A new valid and frozen instance of an Address
     */
    function payingTo(script: {
        (from?: any): import("./script/script");
        new (from?: any): import("./script/script");
        fromBuffer(buffer: any): import("./script/script");
        fromASM(str: any): import("./script/script");
        fromHex(str: any): import("./script/script");
        fromString(str: any): import("./script/script");
        types: typeof import("./script/script").types;
        OP_RETURN_STANDARD_SIZE: number;
        outputIdentifiers: typeof import("./script/script").outputIdentifiers;
        inputIdentifiers: typeof import("./script/script").inputIdentifiers;
        buildEscrowOut(inputPublicKeys: PublicKey[], reclaimPublicKey: PublicKey): import("./script/script");
        buildMultisigOut(publicKeys: PublicKey[], threshold: number, opts?: any): import("./script/script");
        buildMultisigIn(pubkeys: PublicKey[], threshold: number, signatures: any[], opts?: any): import("./script/script");
        buildP2SHMultisigIn(pubkeys: PublicKey[], threshold: number, signatures: any[], opts?: any): import("./script/script");
        buildPublicKeyHashOut(to: PublicKey | Address): import("./script/script");
        buildScriptTemplateOut(to: string | PublicKey | Address): import("./script/script");
        buildGroupedPublicKeyTemplateOut(to: string | PublicKey | Address, groupId: Buffer, groupAmount: Buffer): import("./script/script");
        buildPublicKeyOut(pubkey: any): import("./script/script");
        buildDataOut(data: string | Buffer, encoding: string): import("./script/script");
        buildScriptHashOut(script: Address | import("./script/script")): import("./script/script");
        buildPublicKeyIn(signature: Buffer | import("./crypto/signature"), sigtype?: number): import("./script/script");
        buildPublicKeyHashIn(publicKey: string | Buffer | PublicKey, signature: Buffer | import("./crypto/signature"), sigtype?: number): import("./script/script");
        buildPublicKeyTemplateIn(publicKey: string | Buffer | PublicKey, signature: Buffer | import("./crypto/signature"), sigtype?: number): import("./script/script");
        buildScriptTemplateIn(template: import("./script/script"), constraint: Opcode | import("./script/script"), satisfier: Buffer, sigtype?: number): import("./script/script");
        buildEscrowIn(publicKey: PublicKey, signature: import("./crypto/signature"), redeemScript: RedeemScript): import("./script/script");
        empty(): import("./script/script");
        fromAddress(address: any): import("./script/script");
        Interpreter: typeof import("./script/interpreter");
    }, network: any): Address;
    /**
     * Extract address from a Script. The script must be of one
     * of the following types: p2pkh input, p2pkh output, p2sh input
     * or p2sh output.
     * This will analyze the script and extract address information from it.
     * If you want to transform any script to a p2sh Address paying
     * to that script's hash instead, use {{Address#payingTo}}
     *
     * @param {Script} script - An instance of Script
     * @param {String|Network} network - either a Network instance, 'livenet', or 'testnet'
     * @returns {Address} A new valid and frozen instance of an Address
    
    Address.fromScript = function(script, network) {
      $.checkArgument(script instanceof Script, 'script must be a Script instance');
      var info = Address._transformScript(script, network);
      return new Address(info.hashBuffer, network, info.type);
    };
     */
    /**
     * Instantiate an address from an address string
     *
     * @param {string} str - An string of the bitcoin address
     * @param {Networks.Network} network - either a Network instance, 'livenet', or 'testnet'
     * @param {string=} type - The type of address: 'script' or 'pubkey'
     * @returns {Address} A new valid and frozen instance of an Address
     */
    function fromString(str: string, network: Networks.Network, type?: string): Address;
    /**
     * Internal function to transform a nexa address string
     *
     * @param {string} data
     * @param {String|Network=} network - either a Network instance, 'livenet', or 'testnet'
     * @param {string=} type - The type: 'pubkeyhash' or 'scripthash'
     * @returns {Object} An object with keys: hashBuffer, network and type
     * @private
     */
    function _transformString(data: string, network?: any, type?: string): any;
    /**
     * Instantiate an address from an Object
     *
     * @param {string} json - An JSON string or Object with keys: hash, network and type
     * @returns {Address} A new valid instance of an Address
     */
    function fromObject(obj: any): Address;
    /**
     * Will return a validation error if exists
     *
     * @example
     * ```javascript
     * // a network mismatch error
     * var error = Address.getValidationError('15vkcKf7gB23wLAnZLmbVuMiiVDc1Nm4a2', 'testnet');
     * ```
     *
     * @param {string|Buffer|Uint8Array} data - The encoded data
     * @param {Networks.Network} network - either a Network instance, 'livenet', or 'testnet'
     * @param {string} type - The type of address: 'TEMPLATE' or 'GROUP' or 'P2PKH'
     * @returns {null|Error} The corresponding error message
     */
    function getValidationError(data: string | Uint8Array | Buffer, network: Networks.Network, type: string): Error;
    /**
     * Will return a boolean if an address is valid
     *
     * @example
     * ```javascript
     * assert(Address.isValid('15vkcKf7gB23wLAnZLmbVuMiiVDc1Nm4a2', 'livenet'));
     * ```
     *
     * @param {string|Buffer|Uint8Array} data - The encoded data
     * @param {Networks.Network} network - either a Network instance, 'livenet', or 'testnet'
     * @param {string} type - The type of address: 'TEMPLATE' or 'GROUP' or 'P2PKH'
     * @returns {boolean} The corresponding error message
     */
    function isValid(data: string | Uint8Array | Buffer, network?: Networks.Network, type?: string): boolean;
    /**
     *
     * @param {string} address
     * @returns {Address} A new valid and frozen instance of an Address
     */
    function decodeNexaAddress(address: string): Address;
    /**
     * Will return the transaction output type by the address type
     *
     * @param {string} address
     * @returns {number} 1 - Template, 0 - otherwise
     */
    function getOutputType(address: string): number;
}
import Networks = require("./networks");
import Opcode = require("./opcode");
import PublicKey = require("./publickey");
//# sourceMappingURL=address.d.ts.map