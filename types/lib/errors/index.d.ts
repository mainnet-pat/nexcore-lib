export = nexcore.Error;
declare class Error {
    private constructor();
    name: string;
}
declare namespace Error {
    export { extend };
}
declare namespace nexcore { }
declare function extend(spec: any): Error;
//# sourceMappingURL=index.d.ts.map