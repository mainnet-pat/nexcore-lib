export = Schnorr;
declare function Schnorr(obj: any): Schnorr;
declare class Schnorr {
    constructor(obj: any);
    set(obj: any): this;
    hashbuf: any;
    endian: any;
    privkey: any;
    pubkey: any;
    sig: any;
    verified: any;
    privkey2pubkey(): void;
    toPublicKey(): any;
    sign(): this;
    /**
     * Schnorr implementation used from bitcoinabc at https://reviews.bitcoinabc.org/D2501
     */
    _findSignature(d: any, e: any): {
        r: any;
        s: any;
    };
    sigError(): boolean | "hashbuf must be a 32 byte buffer" | "signature must be a 64 byte or 65 byte array";
    verify(): this;
    /**
     * RFC6979 deterministic nonce generation used from https://reviews.bitcoinabc.org/D2501
     * @param {Buffer} privkeybuf
     * @param {Buffer} msgbuf
     * @return k {BN}
     */
    nonceFunctionRFC6979(privkey: any, msgbuf: Buffer): any;
}
declare namespace Schnorr {
    function sign(hashbuf: any, privkey: any, endian: any): any;
    function verify(hashbuf: any, sig: any, pubkey: any, endian: any): any;
}
//# sourceMappingURL=schnorr.d.ts.map