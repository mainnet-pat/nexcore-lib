export = Point;
/**
 *
 * Instantiate a valid secp256k1 Point from the X and Y coordinates.
 *
 * @param {BN|String} x - The X coordinate
 * @param {BN|String} y - The Y coordinate
 * @link https://github.com/indutny/elliptic
 * @augments elliptic.curve.point
 * @throws {Error} A validation error if exists
 * @returns {Point} An instance of Point
 * @constructor
 */
declare function Point(x: BN | string, y: BN | string, isRed: any): Point;
declare class Point {
    /**
     *
     * Instantiate a valid secp256k1 Point from the X and Y coordinates.
     *
     * @param {BN|String} x - The X coordinate
     * @param {BN|String} y - The Y coordinate
     * @link https://github.com/indutny/elliptic
     * @augments elliptic.curve.point
     * @throws {Error} A validation error if exists
     * @returns {Point} An instance of Point
     * @constructor
     */
    constructor(x: BN | string, y: BN | string, isRed: any);
    _getX: any;
    /**
     *
     * Will return the X coordinate of the Point
     *
     * @returns {BN} A BN instance of the X coordinate
     */
    getX(): BN;
    _getY: any;
    /**
     *
     * Will return the Y coordinate of the Point
     *
     * @returns {BN} A BN instance of the Y coordinate
     */
    getY(): BN;
    /**
     *
     * Will determine if the point is valid
     *
     * @link https://www.iacr.org/archive/pkc2003/25670211/25670211.pdf
     * @param {Point} An instance of Point
     * @throws {Error} A validation error if exists
     * @returns {Point} An instance of the same Point
     */
    validate(): Point;
    hasSquare(): any;
    isSquare(x: any): any;
}
declare namespace Point {
    /**
     *
     * Instantiate a valid secp256k1 Point from only the X coordinate
     *
     * @param {boolean} odd - If the Y coordinate is odd
     * @param {BN|String} x - The X coordinate
     * @throws {Error} A validation error if exists
     * @returns {Point} An instance of Point
     */
    function fromX(odd: boolean, x: any): Point;
    /**
     *
     * Will return a secp256k1 ECDSA base point.
     *
     * @link https://en.bitcoin.it/wiki/Secp256k1
     * @returns {Point} An instance of the base point.
     */
    function getG(): Point;
    /**
     *
     * Will return the max of range of valid private keys as governed by the secp256k1 ECDSA standard.
     *
     * @link https://en.bitcoin.it/wiki/Private_key#Range_of_valid_ECDSA_private_keys
     * @returns {BN} A BN instance of the number of points on the curve
     */
    function getN(): BN;
    function pointToCompressed(point: any): Buffer;
}
//# sourceMappingURL=point.d.ts.map