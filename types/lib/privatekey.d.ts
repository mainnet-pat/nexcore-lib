export = PrivateKey;
/**
 * Instantiate a PrivateKey from a BN, Buffer and WIF.
 *
 * @example
 * ```javascript
 * // generate a new random key
 * var key = PrivateKey();
 *
 * // get the associated address
 * var address = key.toAddress();
 *
 * // encode into wallet export format
 * var exported = key.toWIF();
 *
 * // instantiate from the exported (and saved) private key
 * var imported = PrivateKey.fromWIF(exported);
 * ```
 *
 * @param {string} data - The encoded data in various formats
 * @param {Network|string=} network - a {@link Network} object, or a string with the network name
 * @returns {PrivateKey} A new valid instance of an PrivateKey
 * @constructor
 */
declare function PrivateKey(data: string, network?: (Network | string) | undefined): PrivateKey;
declare class PrivateKey {
    /**
     * Instantiate a PrivateKey from a BN, Buffer and WIF.
     *
     * @example
     * ```javascript
     * // generate a new random key
     * var key = PrivateKey();
     *
     * // get the associated address
     * var address = key.toAddress();
     *
     * // encode into wallet export format
     * var exported = key.toWIF();
     *
     * // instantiate from the exported (and saved) private key
     * var imported = PrivateKey.fromWIF(exported);
     * ```
     *
     * @param {string} data - The encoded data in various formats
     * @param {Network|string=} network - a {@link Network} object, or a string with the network name
     * @returns {PrivateKey} A new valid instance of an PrivateKey
     * @constructor
     */
    constructor(data: string, network?: (Network | string) | undefined);
    /**
     * Internal helper to instantiate PrivateKey internal `info` object from
     * different kinds of arguments passed to the constructor.
     *
     * @param {*} data
     * @param {Network|string=} network - a {@link Network} object, or a string with the network name
     * @return {Object}
     */
    _classifyArguments(data: any, network?: (Network | string) | undefined): any;
    /**
     * Will output the PrivateKey encoded as hex string
     *
     * @returns {string}
     */
    toString(): string;
    /**
     * Will output the PrivateKey to a WIF string
     *
     * @returns {string} A WIP representation of the private key
     */
    toWIF(): string;
    /**
     * Will return the private key as a BN instance
     *
     * @returns {BN} A BN instance of the private key
     */
    toBigNumber(): BN;
    /**
     * Will return the private key as a BN buffer
     *
     * @returns {Buffer} A buffer of the private key
     */
    toBuffer(): Buffer;
    /**
     * WARNING: This method will not be officially supported until v1.0.0.
     *
     *
     * Will return the private key as a BN buffer without leading zero padding
     *
     * @returns {Buffer} A buffer of the private key
     */
    toBufferNoPadding(): Buffer;
    /**
     * Will return the corresponding public key
     *
     * @returns {PublicKey} A public key generated from the private key
     */
    toPublicKey(): PublicKey;
    _pubkey: PublicKey;
    /**
     * Will return an address for the private key
     * @param {Network=} network - optional parameter specifying
     * the desired network for the address
     *
     * @returns {Address} An address generated from the private key
     */
    toAddress(network?: Network): Address;
    /**
     * @returns {Object} A plain object representation
     */
    toObject: () => any;
    toJSON(): any;
    /**
     * Will return a string formatted for the console
     *
     * @returns {string} Private key
     */
    inspect(): string;
}
declare namespace PrivateKey {
    /**
     * Internal function to get a random Big Number (BN)
     *
     * @returns {BN} A new randomly generated BN
     * @private
     */
    function _getRandomBN(): BN;
    /**
     * Internal function to transform a WIF Buffer into a private key
     *
     * @param {Buffer} buf - An WIF string
     * @param {Network|string=} network - a {@link Network} object, or a string with the network name
     * @returns {Object} An object with keys: bn, network and compressed
     * @private
     */
    function _transformBuffer(buf: Buffer, network?: any): any;
    /**
     * Internal function to transform a BN buffer into a private key
     *
     * @param {Buffer} buf
     * @param {Network|string=} network - a {@link Network} object, or a string with the network name
     * @returns {object} an Object with keys: bn, network, and compressed
     * @private
     */
    function _transformBNBuffer(buf: Buffer, network?: any): any;
    /**
     * Internal function to transform a WIF string into a private key
     *
     * @param {string} buf - An WIF string
     * @returns {Object} An object with keys: bn, network and compressed
     * @private
     */
    function _transformWIF(str: any, network: any): any;
    /**
     * Instantiate a PrivateKey from a Buffer with the DER or WIF representation
     *
     * @param {Buffer} arg
     * @param {Network} network
     * @return {PrivateKey}
     */
    function fromBuffer(arg: Buffer, network: Network): PrivateKey;
    /**
     * Internal function to transform a JSON string on plain object into a private key
     * return this.
     *
     * @param {string} json - A JSON string or plain object
     * @returns {Object} An object with keys: bn, network and compressed
     * @private
     */
    function _transformObject(json: string): any;
    function fromString(str: string): PrivateKey;
    function fromWIF(str: string): PrivateKey;
    /**
     * Instantiate a PrivateKey from a plain JavaScript object
     *
     * @param {Object} obj - The output from privateKey.toObject()
     */
    function fromObject(obj: any): PrivateKey;
    /**
     * Instantiate a PrivateKey from random bytes
     *
     * @param {string=} network - Either "livenet" or "testnet"
     * @returns {PrivateKey} A new valid instance of PrivateKey
     */
    function fromRandom(network?: string): PrivateKey;
    /**
     * Check if there would be any errors when initializing a PrivateKey
     *
     * @param {string} data - The encoded data in various formats
     * @param {string=} network - Either "livenet" or "testnet"
     * @returns {null|Error} An error if exists
     */
    function getValidationError(data: string, network?: string): Error;
    /**
     * Check if the parameters are valid
     *
     * @param {string} data - The encoded data in various formats
     * @param {string=} network - Either "livenet" or "testnet"
     * @returns {Boolean} If the private key is would be valid
     */
    function isValid(data: string, network?: string): boolean;
}
import PublicKey = require("./publickey");
import Address = require("./address");
//# sourceMappingURL=privatekey.d.ts.map