export = Interpreter;
/**
 * Bitcoin transactions contain scripts. Each input has a script called the
 * scriptSig, and each output has a script called the scriptPubkey. To validate
 * an input, the input's script is concatenated with the referenced output script,
 * and the result is executed. If at the end of execution the stack contains a
 * "true" value, then the transaction is valid.
 *
 * The primary way to use this class is via the verify function.
 * e.g., Interpreter().verify( ... );
 */
declare function Interpreter(obj: any): Interpreter;
declare class Interpreter {
    /**
     * Bitcoin transactions contain scripts. Each input has a script called the
     * scriptSig, and each output has a script called the scriptPubkey. To validate
     * an input, the input's script is concatenated with the referenced output script,
     * and the result is executed. If at the end of execution the stack contains a
     * "true" value, then the transaction is valid.
     *
     * The primary way to use this class is via the verify function.
     * e.g., Interpreter().verify( ... );
     */
    constructor(obj: any);
    /**
     * Verifies a Script by executing it and returns true if it is valid.
     * This function needs to be provided with the scriptSig and the scriptPubkey
     * separately.
     * @param {Script} scriptSig - the script's first part (corresponding to the tx input)
     * @param {Script} scriptPubkey - the script's last part (corresponding to the tx output)
     * @param {Transaction=} tx - the Transaction containing the scriptSig in one input (used
     *    to check signature validity for some opcodes like OP_CHECKSIG)
     * @param {number} nin - index of the transaction input containing the scriptSig verified.
     * @param {number} flags - evaluation flags. See Interpreter.SCRIPT_* constants
     * @param {number} satoshisBN - amount in satoshis of the input to be verified (when FORKID signhash is used)
     *
     * Translated from bitcoind's VerifyScript
     */
    verify(scriptSig: Script, scriptPubkey: Script, tx?: {
        (serialized: any): import("../transaction/transaction");
        new (serialized: any): import("../transaction/transaction");
        DUST_AMOUNT: number;
        FEE_SECURITY_MARGIN: number;
        MAX_MONEY: number;
        NLOCKTIME_BLOCKHEIGHT_LIMIT: number;
        NLOCKTIME_MAX_VALUE: number;
        FEE_PER_KB: number;
        CHANGE_OUTPUT_MAX_SIZE: number;
        shallowCopy(transaction: import("../transaction/transaction")): import("../transaction/transaction");
        Input: {
            (params: any): import("../transaction/input/input");
            new (params: any): import("../transaction/input/input");
            MAXINT: number;
            DEFAULT_SEQNUMBER: number;
            DEFAULT_LOCKTIME_SEQNUMBER: number;
            DEFAULT_RBF_SEQNUMBER: number;
            SEQUENCE_LOCKTIME_TYPE_FLAG: number;
            DEFAULT_TYPE: number;
            fromObject(obj: any): import("../transaction/input/input");
            fromBufferReader(br: any): import("../transaction/input/input");
            PublicKey: typeof import("../transaction/input/publickey");
            PublicKeyHash: typeof import("../transaction/input/publickeyhash");
            PublicKeyTemplate: typeof import("../transaction/input/publickeytemplate");
            MultiSig: typeof import("../transaction/input/multisig");
            MultiSigScriptHash: typeof import("../transaction/input/multisigscripthash");
            Escrow: typeof import("../transaction/input/escrow");
            ScriptTemplate: typeof import("../transaction/input/scripttemplate"); /**
             * Bitcoin transactions contain scripts. Each input has a script called the
             * scriptSig, and each output has a script called the scriptPubkey. To validate
             * an input, the input's script is concatenated with the referenced output script,
             * and the result is executed. If at the end of execution the stack contains a
             * "true" value, then the transaction is valid.
             *
             * The primary way to use this class is via the verify function.
             * e.g., Interpreter().verify( ... );
             */
        };
        Output: typeof import("../transaction/output");
        UnspentOutput: typeof import("../transaction/unspentoutput");
        Signature: typeof import("../transaction/signature");
        Sighash: {
            sighash: (transaction: import("../transaction/transaction"), sighashType: number, inputNumber: number, subscript: {
                (from?: any): Script;
                new (from?: any): Script;
                fromBuffer(buffer: any): Script;
                fromASM(str: any): Script;
                fromHex(str: any): Script;
                fromString(str: any): Script;
                types: typeof Script.types;
                OP_RETURN_STANDARD_SIZE: number;
                outputIdentifiers: typeof Script.outputIdentifiers;
                inputIdentifiers: typeof Script.inputIdentifiers;
                buildEscrowOut(inputPublicKeys: PublicKey[], reclaimPublicKey: PublicKey): Script;
                buildMultisigOut(publicKeys: PublicKey[], threshold: number, opts?: any): Script;
                buildMultisigIn(pubkeys: PublicKey[], threshold: number, signatures: any[], opts?: any): Script;
                buildP2SHMultisigIn(pubkeys: PublicKey[], threshold: number, signatures: any[], opts?: any): Script;
                buildPublicKeyHashOut(to: PublicKey | import("../address")): Script;
                buildScriptTemplateOut(to: string | PublicKey | import("../address")): Script;
                buildGroupedPublicKeyTemplateOut(to: string | PublicKey | import("../address"), groupId: Buffer, groupAmount: Buffer): Script;
                buildPublicKeyOut(pubkey: any): Script;
                buildDataOut(data: string | Buffer, encoding: string): Script;
                buildScriptHashOut(script: import("../address") | Script): Script;
                buildPublicKeyIn(signature: Buffer | Signature, sigtype?: number): Script;
                buildPublicKeyHashIn(publicKey: string | Buffer | PublicKey, signature: Buffer | Signature, sigtype?: number): Script;
                buildPublicKeyTemplateIn(publicKey: string | Buffer | PublicKey, signature: Buffer | Signature, sigtype?: number): Script;
                buildScriptTemplateIn(template: Script, constraint: Opcode | Script, satisfier: Buffer, sigtype?: number): Script;
                buildEscrowIn(publicKey: PublicKey, signature: Signature, redeemScript: RedeemScript): Script;
                empty(): Script;
                fromAddress(address: any): Script;
                Interpreter: typeof Interpreter;
            }, satoshisBN: any, flags: any) => Buffer;
            sign: (transaction: Transaction, privateKey: PrivateKey, sighashType: any, inputIndex: number, subscript: {
                (from?: any): Script;
                new (from?: any): Script;
                fromBuffer(buffer: any): Script;
                fromASM(str: any): Script;
                fromHex(str: any): Script;
                fromString(str: any): Script;
                types: typeof Script.types;
                OP_RETURN_STANDARD_SIZE: number;
                outputIdentifiers: typeof Script.outputIdentifiers;
                inputIdentifiers: typeof Script.inputIdentifiers;
                buildEscrowOut(inputPublicKeys: PublicKey[], reclaimPublicKey: PublicKey): Script;
                buildMultisigOut(publicKeys: PublicKey[], threshold: number, opts?: any): Script;
                buildMultisigIn(pubkeys: PublicKey[], threshold: number, signatures: any[], opts?: any): Script;
                buildP2SHMultisigIn(pubkeys: PublicKey[], threshold: number, signatures: any[], opts?: any): Script;
                buildPublicKeyHashOut(to: PublicKey | import("../address")): Script;
                buildScriptTemplateOut(to: string | PublicKey | import("../address")): Script;
                buildGroupedPublicKeyTemplateOut(to: string | PublicKey | import("../address"), groupId: Buffer, groupAmount: Buffer): Script;
                buildPublicKeyOut(pubkey: any): Script;
                buildDataOut(data: string | Buffer, encoding: string): Script;
                buildScriptHashOut(script: import("../address") | Script): Script;
                buildPublicKeyIn(signature: Buffer | Signature, sigtype?: number): Script;
                buildPublicKeyHashIn(publicKey: string | Buffer | PublicKey, signature: Buffer | Signature, sigtype?: number): Script;
                buildPublicKeyTemplateIn(publicKey: string | Buffer | PublicKey, signature: Buffer | Signature, sigtype?: number): Script;
                buildScriptTemplateIn(template: Script, constraint: Opcode | Script, satisfier: Buffer, sigtype?: number): Script;
                buildEscrowIn(publicKey: PublicKey, signature: Signature, redeemScript: RedeemScript): Script;
                empty(): Script;
                fromAddress(address: any): Script;
                Interpreter: typeof Interpreter;
            }, satoshisBN: any, flags: any, signingMethod: any) => Signature;
            verify: (transaction: Transaction, signature: Signature, publicKey: PublicKey, inputIndex: number, subscript: {
                (from?: any): Script;
                new (from?: any): Script;
                fromBuffer(buffer: any): Script;
                fromASM(str: any): Script;
                fromHex(str: any): Script;
                fromString(str: any): Script;
                types: typeof Script.types;
                OP_RETURN_STANDARD_SIZE: number;
                outputIdentifiers: typeof Script.outputIdentifiers;
                inputIdentifiers: typeof Script.inputIdentifiers;
                buildEscrowOut(inputPublicKeys: PublicKey[], reclaimPublicKey: PublicKey): Script;
                buildMultisigOut(publicKeys: PublicKey[], threshold: number, opts?: any): Script;
                buildMultisigIn(pubkeys: PublicKey[], threshold: number, signatures: any[], opts?: any): Script;
                buildP2SHMultisigIn(pubkeys: PublicKey[], threshold: number, signatures: any[], opts?: any): Script;
                buildPublicKeyHashOut(to: PublicKey | import("../address")): Script;
                buildScriptTemplateOut(to: string | PublicKey | import("../address")): Script;
                buildGroupedPublicKeyTemplateOut(to: string | PublicKey | import("../address"), groupId: Buffer, groupAmount: Buffer): Script;
                buildPublicKeyOut(pubkey: any): Script;
                buildDataOut(data: string | Buffer, encoding: string): Script;
                buildScriptHashOut(script: import("../address") | Script): Script;
                buildPublicKeyIn(signature: Buffer | Signature, sigtype?: number): Script;
                buildPublicKeyHashIn(publicKey: string | Buffer | PublicKey, signature: Buffer | Signature, sigtype?: number): Script;
                buildPublicKeyTemplateIn(publicKey: string | Buffer | PublicKey, signature: Buffer | Signature, sigtype?: number): Script;
                buildScriptTemplateIn(template: Script, constraint: Opcode | Script, satisfier: Buffer, sigtype?: number): Script;
                buildEscrowIn(publicKey: PublicKey, signature: Signature, redeemScript: RedeemScript): Script;
                empty(): Script;
                fromAddress(address: any): Script;
                Interpreter: typeof Interpreter;
            }, satoshisBN: any, flags: any, signingMethod: any) => boolean;
        };
    } | undefined, nin: number, flags: number, satoshisBN: number): boolean;
    nSigChecks: number;
    errstr: any;
    initialize(obj: any): void;
    stack: any;
    altstack: any;
    pc: any;
    pbegincodehash: any;
    nOpCount: any;
    vfExec: any;
    flags: any;
    set(obj: any): void;
    script: any;
    tx: any;
    nin: any;
    satoshisBN: any;
    /**
     * Translated from bitcoind's CheckSignatureEncoding
     */
    checkRawSignatureEncoding(buf: any): boolean;
    checkSignatureEncoding: (buf: any) => boolean;
    checkTxSignatureEncoding(buf: any): boolean;
    checkDataSignatureEncoding(buf: any): boolean;
    /**
     * Translated from bitcoind's CheckPubKeyEncoding
     */
    checkPubkeyEncoding(buf: any): boolean;
    /**
     * Based on bitcoind's EvalScript function, with the inner loop moved to
     * Interpreter.prototype.step()
     * bitcoind commit: b5d1b1092998bc95313856d535c632ea5a8f9104
     */
    evaluate(): boolean;
    /**
     * Checks a locktime parameter with the transaction's locktime.
     * There are two times of nLockTime: lock-by-blockheight and lock-by-blocktime,
     * distinguished by whether nLockTime < LOCKTIME_THRESHOLD = 500000000
     *
     * See the corresponding code on bitcoin core:
     * https://github.com/bitcoin/bitcoin/blob/ffd75adce01a78b3461b3ff05bcc2b530a9ce994/src/script/interpreter.cpp#L1129
     *
     * @param {BN} nLockTime the locktime read from the script
     * @return {boolean} true if the transaction's locktime is less than or equal to
     *                   the transaction's locktime
     */
    checkLockTime(nLockTime: BN): boolean;
    /**
     * Checks a sequence parameter with the transaction's sequence.
     * @param {BN} nSequence the sequence read from the script
     * @return {boolean} true if the transaction's sequence is less than or equal to
     *                   the transaction's sequence
     */
    checkSequence(nSequence: BN): boolean;
    /**
     * Based on the inner loop of bitcoind's EvalScript function
     * bitcoind commit: b5d1b1092998bc95313856d535c632ea5a8f9104
     */
    step(): boolean;
}
declare namespace Interpreter {
    let _true: Buffer;
    export { _true as true };
    let _false: Buffer;
    export { _false as false };
    export let MAX_SCRIPT_ELEMENT_SIZE: number;
    export let MAXIMUM_ELEMENT_SIZE: number;
    export let LOCKTIME_THRESHOLD: number;
    export let LOCKTIME_THRESHOLD_BN: any;
    export let SCRIPT_VERIFY_NONE: number;
    export let SCRIPT_VERIFY_P2SH: number;
    export let SCRIPT_VERIFY_STRICTENC: number;
    export let SCRIPT_VERIFY_DERSIG: number;
    export let SCRIPT_VERIFY_LOW_S: number;
    export let SCRIPT_VERIFY_NULLDUMMY: number;
    export let SCRIPT_VERIFY_SIGPUSHONLY: number;
    export let SCRIPT_VERIFY_MINIMALDATA: number;
    export let SCRIPT_VERIFY_DISCOURAGE_UPGRADABLE_NOPS: number;
    export let SCRIPT_VERIFY_CLEANSTACK: number;
    export let SCRIPT_VERIFY_CHECKLOCKTIMEVERIFY: number;
    export let SCRIPT_VERIFY_CHECKSEQUENCEVERIFY: number;
    export let SCRIPT_VERIFY_MINIMALIF: number;
    export let SCRIPT_VERIFY_NULLFAIL: number;
    export let SCRIPT_VERIFY_COMPRESSED_PUBKEYTYPE: number;
    export let SCRIPT_ENABLE_SIGHASH_FORKID: number;
    export let SCRIPT_ENABLE_REPLAY_PROTECTION: number;
    export let SCRIPT_ENABLE_CHECKDATASIG: number;
    export let SCRIPT_DISALLOW_SEGWIT_RECOVERY: number;
    export let SCRIPT_ENABLE_SCHNORR_MULTISIG: number;
    export let SEQUENCE_LOCKTIME_DISABLE_FLAG: number;
    export let SEQUENCE_LOCKTIME_TYPE_FLAG: number;
    export let SEQUENCE_LOCKTIME_MASK: number;
    export let SCRIPT_VERIFY_INPUT_SIGCHECKS: number;
    export let SCRIPT_ENFORCE_SIGCHECKS: number;
    export let SCRIPT_64_BIT_INTEGERS: number;
    export let SCRIPT_NATIVE_INTROSPECTION: number;
    export function castToBool(buf: any): boolean;
    export function isSchnorrSig(buf: any): boolean;
    /**
      *
      * Check the buffer is minimally encoded (see https://github.com/bitcoincashorg/spec/blob/master/may-2018-reenabled-opcodes.md#op_bin2num)
      *
      *
      */
    export function _isMinimallyEncoded(buf: any, nMaxNumSize: any): boolean;
    /**
      *
      * minimally encode the buffer content
      *
      * @param {number} nMaxNumSize (max allowed size)
      */
    export function _minimallyEncode(buf: any): any;
}
import Script = require("./script");
import PublicKey = require("../publickey");
import Signature = require("../crypto/signature");
import Opcode = require("../opcode");
//# sourceMappingURL=interpreter.d.ts.map