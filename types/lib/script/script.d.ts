/// <reference types="node" />
export = Script;
/**
 * A bitcoin transaction script. Each transaction's inputs and outputs
 * has a script that is evaluated to validate it's spending.
 *
 * See https://en.bitcoin.it/wiki/Script
 *
 * @constructor
 * @param {Object|string|Buffer=} from optional data to populate script
 */
declare function Script(from?: (any | string | Buffer) | undefined): Script;
declare class Script {
    /**
     * A bitcoin transaction script. Each transaction's inputs and outputs
     * has a script that is evaluated to validate it's spending.
     *
     * See https://en.bitcoin.it/wiki/Script
     *
     * @constructor
     * @param {Object|string|Buffer=} from optional data to populate script
     */
    constructor(from?: (any | string | Buffer) | undefined);
    chunks: any[];
    set(obj: any): this;
    toBuffer(): buffer.Buffer;
    _chunkToString(chunk: any, type: any): string;
    toASM(): string;
    toString(): string;
    toHex(): string;
    inspect(): string;
    /**
     * @returns {boolean} if this is a pay to script template output push script
     */
    isScriptTemplatePushOut(): boolean;
    /**
     * @returns {boolean} if this is a pay to script template output script
     */
    isScriptTemplateOut(): boolean;
    /**
     * @returns {boolean} if this is a pay to script template input script
     */
    isScriptTemplateIn(): boolean;
    /**
     * @returns {boolean} if this is a pay to pubkey template output push script
     */
    isPublicKeyTemplatePushOut(): boolean;
    /**
     * @returns {boolean} if this is a pay to pubkey template output script
     */
    isPublicKeyTemplateOut(): boolean;
    /**
     * @returns {boolean} if this is a grouped pay to pubkey template output script
     */
    isGroupedPublicKeyTemplateOut(): boolean;
    /**
     * @returns {boolean} if this is a pay to public key template input script
     */
    isPublicKeyTemplateIn(): boolean;
    /**
     * @returns {boolean} if this is a pay to pubkey hash output script
     */
    isPublicKeyHashOut(): boolean;
    /**
     * @returns {boolean} if this is a pay to public key hash input script
     */
    isPublicKeyHashIn(): boolean;
    getPublicKey(): any;
    getPublicKeyHash(): any;
    getPublicKeyTemplate(): any;
    /**
     * @returns {boolean} if this is a public key output script
     */
    isPublicKeyOut(): boolean;
    /**
     * @returns {boolean} if this is a pay to public key input script
     */
    isPublicKeyIn(): boolean;
    /**
     * @param {Object=} values - The return values
     * @param {Number} values.version - Set with the witness version
     * @param {Buffer} values.program - Set with the witness program
     * @returns {boolean} if this is a p2wpkh output script
     */
    isWitnessProgram(values?: any | undefined): boolean;
    /**
     * @returns {boolean} if this is a p2sh output script
     */
    isScriptHashOut(): boolean;
    /**
     * @returns {boolean} if this is a p2sh input script
     * Note that these are frequently indistinguishable from pubkeyhashin
     */
    isScriptHashIn(): boolean;
    /**
     * @returns {boolean} if this is a mutlsig output script
     */
    isMultisigOut(): boolean;
    /**
     * @returns {boolean} if this is a multisig input script
     */
    isMultisigIn(): boolean;
    /**
     * @returns {boolean} true if this is a valid standard OP_RETURN output
     */
    isDataOut(): boolean;
    /**
     * Retrieve the associated data for this script.
     * In the case of a pay to public key hash/template or P2SH, return the hash.
     * In the case of a standard OP_RETURN, return the data.
     * In the case of a template push script, return the pushed script as buffer.
     * In the case of a script template, return object with template hash, constraint as hash or op_false, and visible args as script if exist.
     * In the case of a grouped public key template, return object with group id buffer, amount buffer, and the hash
     * @returns {Buffer|object} Buffer or Object in case of script template or grouped
     */
    getData(): Buffer | object;
    /**
     * @returns {boolean} if the script is only composed of data pushing
     * opcodes or small int opcodes (OP_0, OP_1, ..., OP_16)
     */
    isPushOnly(): boolean;
    /**
     * @returns {object} The Script type if it is a known form,
     * or Script.UNKNOWN if it isn't
     */
    classify(): object;
    /**
     * @returns {object} The Script type if it is a known form,
     * or Script.UNKNOWN if it isn't
     */
    classifyOutput(): object;
    /**
     * @returns {object} The Script type if it is a known form,
     * or Script.UNKNOWN if it isn't
     */
    classifyInput(): object;
    /**
     * @returns {boolean} if script is one of the known types
     */
    isStandard(): boolean;
    /**
     * Adds a script element at the start of the script.
     * @param {*} obj a string, number, Opcode, Buffer, or object to add
     * @returns {Script} this script instance
     */
    prepend(obj: any): Script;
    /**
     * Compares a script with another script
     */
    equals(script: any): boolean;
    /**
     * Adds a script element to the end of the script.
     *
     * @param {*} obj a string, number, Opcode, Buffer, or object to add
     * @returns {Script} this script instance
     *
     */
    add(obj: any): Script;
    _addByType(obj: any, prepend: any): void;
    _insertAtPosition(op: any, prepend: any): void;
    _addOpcode(opcode: any, prepend: any): this;
    _addBuffer(buf: any, prepend: any): this;
    removeCodeseparators(): this;
    /**
     * @returns {Script} a new pay to script hash script that pays to this script
     */
    toScriptHashOut(): Script;
    /**
     * Will return the associated address information object
     * @return {Address|boolean}
     */
    getAddressInfo(opts: any): Address | boolean;
    private _getOutputAddressInfo;
    private _getInputAddressInfo;
    /**
     * @param {Network=} network
     * @return {Address|boolean} the associated address for this script if possible, or false
     */
    toAddress(network?: Network): Address | boolean;
    /**
     * Analogous to bitcoind's FindAndDelete. Find and delete equivalent chunks,
     * typically used with push data chunks.  Note that this will find and delete
     * not just the same data, but the same data with the same push data op as
     * produced by default. i.e., if a pushdata in a tx does not use the minimal
     * pushdata op, then when you try to remove the data it is pushing, it will not
     * be removed, because they do not use the same pushdata op.
     */
    findAndDelete(script: any): this;
    /**
     * Comes from bitcoind's script interpreter CheckMinimalPush function
     * @returns {boolean} if the chunk {i} is the smallest way to push that particular data.
     */
    checkMinimalPush(i: any): boolean;
    /**
     * Comes from bitcoind's script DecodeOP_N function
     * @param {number} opcode
     * @returns {number} numeric value in range of 0 to 16
     */
    _decodeOP_N(opcode: number): number;
    /**
     * Comes from bitcoind's script GetSigOpCount(boolean) function
     * @param {boolean} use current (true) or pre-version-0.6 (false) logic
     * @returns {number} number of signature operations required by this script
     */
    getSignatureOperationsCount(accurate: any): number;
}
declare namespace Script {
    function fromBuffer(buffer: any): Script;
    function fromASM(str: any): Script;
    function fromHex(str: any): Script;
    function fromString(str: any): Script;
    namespace types {
        let UNKNOWN: string;
        let PUBKEY_OUT: string;
        let PUBKEY_IN: string;
        let PUBKEYHASH_OUT: string;
        let PUBKEYHASH_IN: string;
        let PUBKEYTEMPLATE_OUT: string;
        let PUBKEYTEMPLATE_IN: string;
        let PUBKEYTEMPLATE_PUSH_OUT: string;
        let GROUPED_PUBKEYTEMPLATE_OUT: string;
        let GROUPED_PUBKEYTEMPLATE_IN: string;
        let SCRIPTHASH_OUT: string;
        let SCRIPTHASH_IN: string;
        let MULTISIG_OUT: string;
        let MULTISIG_IN: string;
        let DATA_OUT: string;
    }
    let OP_RETURN_STANDARD_SIZE: number;
    namespace outputIdentifiers {
        let PUBKEY_OUT_1: any;
        export { PUBKEY_OUT_1 as PUBKEY_OUT };
        let PUBKEYHASH_OUT_1: any;
        export { PUBKEYHASH_OUT_1 as PUBKEYHASH_OUT };
        let PUBKEYTEMPLATE_OUT_1: any;
        export { PUBKEYTEMPLATE_OUT_1 as PUBKEYTEMPLATE_OUT };
        let PUBKEYTEMPLATE_PUSH_OUT_1: any;
        export { PUBKEYTEMPLATE_PUSH_OUT_1 as PUBKEYTEMPLATE_PUSH_OUT };
        let GROUPED_PUBKEYTEMPLATE_OUT_1: any;
        export { GROUPED_PUBKEYTEMPLATE_OUT_1 as GROUPED_PUBKEYTEMPLATE_OUT };
        export let SCRIPTTEMPLATE_OUT: any;
        export let SCRIPTTEMPLATE_PUSH_OUT: any;
        let MULTISIG_OUT_1: any;
        export { MULTISIG_OUT_1 as MULTISIG_OUT };
        let SCRIPTHASH_OUT_1: any;
        export { SCRIPTHASH_OUT_1 as SCRIPTHASH_OUT };
        let DATA_OUT_1: any;
        export { DATA_OUT_1 as DATA_OUT };
    }
    namespace inputIdentifiers {
        let PUBKEY_IN_1: any;
        export { PUBKEY_IN_1 as PUBKEY_IN };
        let PUBKEYHASH_IN_1: any;
        export { PUBKEYHASH_IN_1 as PUBKEYHASH_IN };
        let PUBKEYTEMPLATE_IN_1: any;
        export { PUBKEYTEMPLATE_IN_1 as PUBKEYTEMPLATE_IN };
        export let SCRIPTTEMPLATE_IN: any;
        let MULTISIG_IN_1: any;
        export { MULTISIG_IN_1 as MULTISIG_IN };
        let SCRIPTHASH_IN_1: any;
        export { SCRIPTHASH_IN_1 as SCRIPTHASH_IN };
    }
    /**
     * @returns {Script} a new escrow output redeem script for given input public keys and reclaim public key
     * @param {PublicKey[]} inputPublicKeys - list of all public keys associated with each P2PKH input of the
     * zero-confirmation escrow transaction
     * @param {PublicKey} reclaimPublicKey - the public key used to reclaim the escrow by the customer
     */
    function buildEscrowOut(inputPublicKeys: PublicKey[], reclaimPublicKey: PublicKey): Script;
    /**
     * @returns {Script} a new Multisig output script for given public keys,
     * requiring m of those public keys to spend
     * @param {PublicKey[]} publicKeys - list of all public keys controlling the output
     * @param {number} threshold - amount of required signatures to spend the output
     * @param {Object=} opts - Several options:
     *        - noSorting: defaults to false, if true, don't sort the given
     *                      public keys before creating the script
     */
    function buildMultisigOut(publicKeys: PublicKey[], threshold: number, opts?: any): Script;
    /**
     * A new Multisig input script for the given public keys, requiring m of those public keys to spend
     *
     * @param {PublicKey[]} pubkeys list of all public keys controlling the output
     * @param {number} threshold amount of required signatures to spend the output
     * @param {Array} signatures and array of signature buffers to append to the script
     * @param {Object=} opts
     * @param {boolean=} opts.noSorting don't sort the given public keys before creating the script (false by default)
     * @param {Script=} opts.cachedMultisig don't recalculate the redeemScript
     *
     * @returns {Script}
     */
    function buildMultisigIn(pubkeys: PublicKey[], threshold: number, signatures: any[], opts?: any): Script;
    /**
     * A new P2SH Multisig input script for the given public keys, requiring m of those public keys to spend
     *
     * @param {PublicKey[]} pubkeys list of all public keys controlling the output
     * @param {number} threshold amount of required signatures to spend the output
     * @param {Array} signatures and array of signature buffers to append to the script
     * @param {Object=} opts
     * @param {boolean=} opts.noSorting don't sort the given public keys before creating the script (false by default)
     * @param {Script=} opts.cachedMultisig don't recalculate the redeemScript
     * @param {Uint8Array} opts.checkBits bitfield map 1 or 0 to check which signatures to map against public keys for verification in schnorr multisig mode
     * @param {String} opts.signingMethod method with which input will be signed "ecdsa" or "schnorr"
     *
     * @returns {Script}
     */
    function buildP2SHMultisigIn(pubkeys: PublicKey[], threshold: number, signatures: any[], opts?: any): Script;
    /**
     * @returns {Script} a new pay to public key hash output for the given
     * address or public key
     * @param {(Address|PublicKey)} to - destination address or public key
     */
    function buildPublicKeyHashOut(to: PublicKey | Address): Script;
    /**
     * @param {(Address|PublicKey|string)} to - destination address or public key
     * @returns {Script} a new pay to script template output for the given
     * address or public key
     */
    function buildScriptTemplateOut(to: string | PublicKey | Address): Script;
    /**
     * @returns {Script} a new pay to public key template output for the given
     * address or public key
     * @param {Address|PublicKey|string} to - destination address or public key
     * @param {Buffer} groupId - group id buffer
     * @param {Buffer} groupAmount - quantity amount buffer
     */
    function buildGroupedPublicKeyTemplateOut(to: string | PublicKey | Address, groupId: buffer.Buffer, groupAmount: buffer.Buffer): Script;
    /**
     * @returns {Script} a new pay to public key output for the given
     *  public key
     */
    function buildPublicKeyOut(pubkey: any): Script;
    /**
     * @returns {Script} a new OP_RETURN script with data
     * @param {(string|Buffer)} data - the data to embed in the output
     * @param {(string)} encoding - the type of encoding of the string
     */
    function buildDataOut(data: string | buffer.Buffer, encoding: string): Script;
    /**
     * @param {Script|Address} script - the redeemScript for the new p2sh output.
     *    It can also be a p2sh address
     * @returns {Script} new pay to script hash script for given script
     */
    function buildScriptHashOut(script: Address | Script): Script;
    /**
     * Builds a scriptSig (a script for an input) that signs a public key output script.
     *
     * @param {Signature|Buffer} signature - a Signature object, or the signature in DER canonical encoding
     * @param {number=} sigtype - the type of the signature (defaults to SIGHASH_ALL)
     */
    function buildPublicKeyIn(signature: buffer.Buffer | Signature, sigtype?: number): Script;
    /**
     * Builds a scriptSig (a script for an input) that signs a public key hash
     * output script.
     *
     * @param {Buffer|string|PublicKey} publicKey
     * @param {Signature|Buffer} signature - a Signature object, or the signature in DER canonical encoding
     * @param {number=} sigtype - the type of the signature (defaults to SIGHASH_ALL)
     */
    function buildPublicKeyHashIn(publicKey: string | buffer.Buffer | PublicKey, signature: buffer.Buffer | Signature, sigtype?: number): Script;
    /**
     * Builds a scriptSig (a script for an input) that signs a public key template
     * output script.
     *
     * @param {Buffer|string|PublicKey} publicKey
     * @param {Signature|Buffer} signature - a Signature object, or the signature in DER canonical encoding
     * @param {number=} sigtype - the type of the signature (defaults to SIGHASH_ALL)
     */
    function buildPublicKeyTemplateIn(publicKey: string | buffer.Buffer | PublicKey, signature: buffer.Buffer | Signature, sigtype?: number): Script;
    /**
     * Builds a scriptSig (a script for an input) that signs a script template
     * output script.
     *
     * @param {Script} template - the template script
     * @param {Script|Opcode} constraint - the constraint script or OP_FALSE
     * @param {Buffer} satisfier - the signature in DER canonical encoding
     * @param {number=} sigtype - the type of the signature (defaults to SIGHASH_ALL)
     */
    function buildScriptTemplateIn(template: Script, constraint: Opcode | Script, satisfier: buffer.Buffer, sigtype?: number): Script;
    /**
     * Builds a scriptSig (a script for an input) that signs an escrow output script.
     *
     * @param {PublicKey} publicKey
     * @param {Signature} signature - a Signature object
     * @param {RedeemScript} redeemScript - the escrow redeemScript
     */
    function buildEscrowIn(publicKey: PublicKey, signature: Signature, redeemScript: RedeemScript): Script;
    /**
     * @returns {Script} an empty script
     */
    function empty(): Script;
    /**
     * @return {Script} an output script built from the address
     */
    function fromAddress(address: any): Script;
}
import buffer = require("buffer");
import Address = require("../address");
import PublicKey = require("../publickey");
import Signature = require("../crypto/signature");
import Opcode = require("../opcode");
//# sourceMappingURL=script.d.ts.map