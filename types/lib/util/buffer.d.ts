/// <reference types="node" />
import buffer = require("buffer");
export function equals(a: any, b: any): boolean;
export declare function fill(buffer: buffer.Buffer, value: number): buffer.Buffer;
export declare function copy(original: buffer.Buffer): buffer.Buffer;
export declare function isBuffer(arg: any): boolean;
export declare function emptyBuffer(bytes: number): buffer.Buffer;
export declare let concat: (list: readonly Uint8Array[], totalLength?: number) => buffer.Buffer;
export declare function integerAsSingleByteBuffer(integer: number): buffer.Buffer;
export declare function integerAsBuffer(integer: number): buffer.Buffer;
export declare function integerFromBuffer(buffer: buffer.Buffer): number;
export declare function integerFromSingleByteBuffer(buffer: buffer.Buffer): number;
export declare function bufferToHex(buffer: buffer.Buffer): string;
export declare function reverse(param: buffer.Buffer): buffer.Buffer;
export declare let NULL_HASH: buffer.Buffer;
export declare let EMPTY_BUFFER: buffer.Buffer;
export { equals as equal };
//# sourceMappingURL=buffer.d.ts.map