/// <reference types="node" />
export = Base58;
declare function Base58(obj: any): Base58;
declare class Base58 {
    constructor(obj: any);
    set(obj: any): this;
    buf: any;
    fromBuffer(buf: any): this;
    fromString(str: any): this;
    toBuffer(): any;
    toString(): any;
}
declare namespace Base58 {
    function validCharacters(chars: any): any;
    function encode(buf: any): any;
    function decode(str: any): buffer.Buffer;
}
import buffer = require("buffer");
//# sourceMappingURL=base58.d.ts.map