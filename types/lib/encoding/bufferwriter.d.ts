export = BufferWriter;
declare function BufferWriter(obj: any): BufferWriter;
declare class BufferWriter {
    constructor(obj: any);
    bufLen: number;
    bufs: any[];
    set(obj: any): this;
    toBuffer(): Buffer;
    concat(): Buffer;
    write(buf: any): this;
    writeReverse(buf: any): this;
    writeUInt8(n: any): this;
    writeUInt16BE(n: any): this;
    writeUInt16LE(n: any): this;
    writeUInt32BE(n: any): this;
    writeInt32LE(n: any): this;
    writeUInt32LE(n: any): this;
    writeUInt64BEBN(bn: any): this;
    writeUInt64LEBN(bn: any): this;
    writeVarintNum(n: any): this;
    writeVarintBN(bn: any): this;
}
declare namespace BufferWriter {
    function varintBufNum(n: any): Buffer;
    function varintBufBN(bn: any): Buffer;
}
//# sourceMappingURL=bufferwriter.d.ts.map