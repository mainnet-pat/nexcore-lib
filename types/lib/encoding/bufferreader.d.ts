export = BufferReader;
declare function BufferReader(buf: any): BufferReader;
declare class BufferReader {
    constructor(buf: any);
    set(obj: any): this;
    buf: any;
    pos: any;
    eof(): boolean;
    finished: any;
    read(len: any): any;
    readAll(): any;
    readUInt8(): any;
    readUInt16BE(): any;
    readUInt16LE(): any;
    readUInt32BE(): any;
    readUInt32LE(): any;
    readInt32LE(): any;
    readUInt64BEBN(): any;
    readUInt64LEBN(): any;
    readVarintNum(): any;
    /**
     * reads a length prepended buffer
     */
    readVarLengthBuffer(): any;
    readVarintBuf(): any;
    readVarintBN(): any;
    reverse(): this;
    readReverse(len: any): Buffer;
}
//# sourceMappingURL=bufferreader.d.ts.map