/// <reference types="node" />
export = Base58Check;
declare function Base58Check(obj: any): Base58Check;
declare class Base58Check {
    constructor(obj: any);
    set(obj: any): this;
    buf: any;
    fromBuffer(buf: any): this;
    fromString(str: any): this;
    toBuffer(): any;
    toString(): any;
}
declare namespace Base58Check {
    function validChecksum(data: any, checksum: any): boolean;
    function decode(s: any): buffer.Buffer;
    function checksum(buffer: any): buffer.Buffer;
    function encode(buf: any): any;
}
import buffer = require("buffer");
//# sourceMappingURL=base58check.d.ts.map