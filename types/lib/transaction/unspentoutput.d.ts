export = UnspentOutput;
/**
 * Represents an unspent output information: its script, associated amount and address,
 * transaction id and output index.
 *
 * @constructor
 * @param {object} data
 * @param {string} data.txid the previous transaction id
 * @param {string=} data.txId alias for `txid`
 * @param {number} data.vout the index in the transaction
 * @param {number=} data.outputIndex alias for `vout`
 * @param {string|Script} data.scriptPubKey the script that must be resolved to release the funds
 * @param {string|Script=} data.script alias for `scriptPubKey`
 * @param {number} data.amount amount of bitcoins associated
 * @param {number=} data.satoshis alias for `amount`, but expressed in satoshis (1 BTC = 1e8 satoshis)
 * @param {string|Address=} data.address the associated address to the script, if provided
 */
declare function UnspentOutput(data: {
    txid: string;
    txId?: string | undefined;
    vout: number;
    outputIndex?: number | undefined;
    scriptPubKey: string | {
        (from?: any): import("../script/script");
        new (from?: any): import("../script/script");
        fromBuffer(buffer: any): import("../script/script");
        fromASM(str: any): import("../script/script");
        fromHex(str: any): import("../script/script");
        fromString(str: any): import("../script/script");
        types: typeof import("../script/script").types;
        OP_RETURN_STANDARD_SIZE: number;
        outputIdentifiers: typeof import("../script/script").outputIdentifiers;
        inputIdentifiers: typeof import("../script/script").inputIdentifiers;
        buildEscrowOut(inputPublicKeys: import("../publickey")[], reclaimPublicKey: import("../publickey")): import("../script/script");
        buildMultisigOut(publicKeys: import("../publickey")[], threshold: number, opts?: any): import("../script/script");
        buildMultisigIn(pubkeys: import("../publickey")[], threshold: number, signatures: any[], opts?: any): import("../script/script");
        buildP2SHMultisigIn(pubkeys: import("../publickey")[], threshold: number, signatures: any[], opts?: any): import("../script/script");
        buildPublicKeyHashOut(to: import("../publickey") | Address): import("../script/script");
        buildScriptTemplateOut(to: string | import("../publickey") | Address): import("../script/script");
        buildGroupedPublicKeyTemplateOut(to: string | import("../publickey") | Address, groupId: Buffer, groupAmount: Buffer): import("../script/script");
        buildPublicKeyOut(pubkey: any): import("../script/script");
        buildDataOut(data: string | Buffer, encoding: string): import("../script/script");
        buildScriptHashOut(script: Address | import("../script/script")): import("../script/script");
        buildPublicKeyIn(signature: Buffer | import("../crypto/signature"), sigtype?: number): import("../script/script");
        buildPublicKeyHashIn(publicKey: string | Buffer | import("../publickey"), signature: Buffer | import("../crypto/signature"), sigtype?: number): import("../script/script");
        buildPublicKeyTemplateIn(publicKey: string | Buffer | import("../publickey"), signature: Buffer | import("../crypto/signature"), sigtype?: number): import("../script/script");
        buildScriptTemplateIn(template: import("../script/script"), constraint: import("../opcode") | import("../script/script"), satisfier: Buffer, sigtype?: number): import("../script/script");
        buildEscrowIn(publicKey: import("../publickey"), signature: import("../crypto/signature"), redeemScript: RedeemScript): import("../script/script");
        empty(): import("../script/script");
        fromAddress(address: any): import("../script/script");
        Interpreter: typeof import("../script/interpreter");
    };
    script?: (string | {
        (from?: any): import("../script/script");
        new (from?: any): import("../script/script");
        fromBuffer(buffer: any): import("../script/script");
        fromASM(str: any): import("../script/script");
        fromHex(str: any): import("../script/script");
        fromString(str: any): import("../script/script");
        types: typeof import("../script/script").types;
        OP_RETURN_STANDARD_SIZE: number;
        outputIdentifiers: typeof import("../script/script").outputIdentifiers;
        inputIdentifiers: typeof import("../script/script").inputIdentifiers;
        buildEscrowOut(inputPublicKeys: import("../publickey")[], reclaimPublicKey: import("../publickey")): import("../script/script");
        buildMultisigOut(publicKeys: import("../publickey")[], threshold: number, opts?: any): import("../script/script");
        buildMultisigIn(pubkeys: import("../publickey")[], threshold: number, signatures: any[], opts?: any): import("../script/script");
        buildP2SHMultisigIn(pubkeys: import("../publickey")[], threshold: number, signatures: any[], opts?: any): import("../script/script");
        buildPublicKeyHashOut(to: import("../publickey") | Address): import("../script/script");
        buildScriptTemplateOut(to: string | import("../publickey") | Address): import("../script/script");
        buildGroupedPublicKeyTemplateOut(to: string | import("../publickey") | Address, groupId: Buffer, groupAmount: Buffer): import("../script/script");
        buildPublicKeyOut(pubkey: any): import("../script/script");
        buildDataOut(data: string | Buffer, encoding: string): import("../script/script");
        buildScriptHashOut(script: Address | import("../script/script")): import("../script/script");
        buildPublicKeyIn(signature: Buffer | import("../crypto/signature"), sigtype?: number): import("../script/script");
        buildPublicKeyHashIn(publicKey: string | Buffer | import("../publickey"), signature: Buffer | import("../crypto/signature"), sigtype?: number): import("../script/script");
        buildPublicKeyTemplateIn(publicKey: string | Buffer | import("../publickey"), signature: Buffer | import("../crypto/signature"), sigtype?: number): import("../script/script");
        buildScriptTemplateIn(template: import("../script/script"), constraint: import("../opcode") | import("../script/script"), satisfier: Buffer, sigtype?: number): import("../script/script");
        buildEscrowIn(publicKey: import("../publickey"), signature: import("../crypto/signature"), redeemScript: RedeemScript): import("../script/script");
        empty(): import("../script/script");
        fromAddress(address: any): import("../script/script");
        Interpreter: typeof import("../script/interpreter");
    }) | undefined;
    amount: number;
    satoshis?: number | undefined;
    address?: (string | Address) | undefined;
}): UnspentOutput;
declare class UnspentOutput {
    /**
     * Represents an unspent output information: its script, associated amount and address,
     * transaction id and output index.
     *
     * @constructor
     * @param {object} data
     * @param {string} data.txid the previous transaction id
     * @param {string=} data.txId alias for `txid`
     * @param {number} data.vout the index in the transaction
     * @param {number=} data.outputIndex alias for `vout`
     * @param {string|Script} data.scriptPubKey the script that must be resolved to release the funds
     * @param {string|Script=} data.script alias for `scriptPubKey`
     * @param {number} data.amount amount of bitcoins associated
     * @param {number=} data.satoshis alias for `amount`, but expressed in satoshis (1 BTC = 1e8 satoshis)
     * @param {string|Address=} data.address the associated address to the script, if provided
     */
    constructor(data: {
        txid: string;
        txId?: string | undefined;
        vout: number;
        outputIndex?: number | undefined;
        scriptPubKey: string | {
            (from?: any): import("../script/script");
            new (from?: any): import("../script/script");
            fromBuffer(buffer: any): import("../script/script");
            fromASM(str: any): import("../script/script");
            fromHex(str: any): import("../script/script");
            fromString(str: any): import("../script/script");
            types: typeof import("../script/script").types;
            OP_RETURN_STANDARD_SIZE: number;
            outputIdentifiers: typeof import("../script/script").outputIdentifiers;
            inputIdentifiers: typeof import("../script/script").inputIdentifiers;
            buildEscrowOut(inputPublicKeys: import("../publickey")[], reclaimPublicKey: import("../publickey")): import("../script/script");
            buildMultisigOut(publicKeys: import("../publickey")[], threshold: number, opts?: any): import("../script/script");
            buildMultisigIn(pubkeys: import("../publickey")[], threshold: number, signatures: any[], opts?: any): import("../script/script");
            buildP2SHMultisigIn(pubkeys: import("../publickey")[], threshold: number, signatures: any[], opts?: any): import("../script/script");
            buildPublicKeyHashOut(to: import("../publickey") | Address): import("../script/script");
            buildScriptTemplateOut(to: string | import("../publickey") | Address): import("../script/script");
            buildGroupedPublicKeyTemplateOut(to: string | import("../publickey") | Address, groupId: Buffer, groupAmount: Buffer): import("../script/script");
            buildPublicKeyOut(pubkey: any): import("../script/script");
            buildDataOut(data: string | Buffer, encoding: string): import("../script/script");
            buildScriptHashOut(script: Address | import("../script/script")): import("../script/script");
            buildPublicKeyIn(signature: Buffer | import("../crypto/signature"), sigtype?: number): import("../script/script");
            buildPublicKeyHashIn(publicKey: string | Buffer | import("../publickey"), signature: Buffer | import("../crypto/signature"), sigtype?: number): import("../script/script");
            buildPublicKeyTemplateIn(publicKey: string | Buffer | import("../publickey"), signature: Buffer | import("../crypto/signature"), sigtype?: number): import("../script/script");
            buildScriptTemplateIn(template: import("../script/script"), constraint: import("../opcode") | import("../script/script"), satisfier: Buffer, sigtype?: number): import("../script/script");
            buildEscrowIn(publicKey: import("../publickey"), signature: import("../crypto/signature"), redeemScript: RedeemScript): import("../script/script");
            empty(): import("../script/script");
            fromAddress(address: any): import("../script/script");
            Interpreter: typeof import("../script/interpreter");
        };
        script?: (string | {
            (from?: any): import("../script/script");
            new (from?: any): import("../script/script");
            fromBuffer(buffer: any): import("../script/script");
            fromASM(str: any): import("../script/script");
            fromHex(str: any): import("../script/script");
            fromString(str: any): import("../script/script");
            types: typeof import("../script/script").types;
            OP_RETURN_STANDARD_SIZE: number;
            outputIdentifiers: typeof import("../script/script").outputIdentifiers;
            inputIdentifiers: typeof import("../script/script").inputIdentifiers;
            buildEscrowOut(inputPublicKeys: import("../publickey")[], reclaimPublicKey: import("../publickey")): import("../script/script");
            buildMultisigOut(publicKeys: import("../publickey")[], threshold: number, opts?: any): import("../script/script");
            buildMultisigIn(pubkeys: import("../publickey")[], threshold: number, signatures: any[], opts?: any): import("../script/script");
            buildP2SHMultisigIn(pubkeys: import("../publickey")[], threshold: number, signatures: any[], opts?: any): import("../script/script");
            buildPublicKeyHashOut(to: import("../publickey") | Address): import("../script/script");
            buildScriptTemplateOut(to: string | import("../publickey") | Address): import("../script/script");
            buildGroupedPublicKeyTemplateOut(to: string | import("../publickey") | Address, groupId: Buffer, groupAmount: Buffer): import("../script/script");
            buildPublicKeyOut(pubkey: any): import("../script/script");
            buildDataOut(data: string | Buffer, encoding: string): import("../script/script");
            buildScriptHashOut(script: Address | import("../script/script")): import("../script/script");
            buildPublicKeyIn(signature: Buffer | import("../crypto/signature"), sigtype?: number): import("../script/script");
            buildPublicKeyHashIn(publicKey: string | Buffer | import("../publickey"), signature: Buffer | import("../crypto/signature"), sigtype?: number): import("../script/script");
            buildPublicKeyTemplateIn(publicKey: string | Buffer | import("../publickey"), signature: Buffer | import("../crypto/signature"), sigtype?: number): import("../script/script");
            buildScriptTemplateIn(template: import("../script/script"), constraint: import("../opcode") | import("../script/script"), satisfier: Buffer, sigtype?: number): import("../script/script");
            buildEscrowIn(publicKey: import("../publickey"), signature: import("../crypto/signature"), redeemScript: RedeemScript): import("../script/script");
            empty(): import("../script/script");
            fromAddress(address: any): import("../script/script");
            Interpreter: typeof import("../script/interpreter");
        }) | undefined;
        amount: number;
        satoshis?: number | undefined;
        address?: (string | Address) | undefined;
    });
    /**
     * Provide an informative output when displaying this object in the console
     * @returns string
     */
    inspect(): string;
    /**
     * String representation: just "txid:index"
     * @returns string
     */
    toString(): string;
    /**
     * Returns a plain object (no prototype or methods) with the associated info for this output
     * @return {object}
     */
    toObject: () => object;
    toJSON(): object;
}
declare namespace UnspentOutput {
    /**
     * Deserialize an UnspentOutput from an object
     * @param {object|string} data
     * @return UnspentOutput
     */
    function fromObject(data: any): UnspentOutput;
}
import Address = require("../address");
//# sourceMappingURL=unspentoutput.d.ts.map