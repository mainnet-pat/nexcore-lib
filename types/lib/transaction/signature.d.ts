export = TransactionSignature;
/**
 * @desc
 * Wrapper around Signature with fields related to signing a transaction specifically
 *
 * @param {Object|string|TransactionSignature} arg
 * @constructor
 */
declare function TransactionSignature(arg: any | string | TransactionSignature): TransactionSignature;
declare class TransactionSignature {
    /**
     * @desc
     * Wrapper around Signature with fields related to signing a transaction specifically
     *
     * @param {Object|string|TransactionSignature} arg
     * @constructor
     */
    constructor(arg: any | string | TransactionSignature);
    _fromObject(arg: any): this;
    publicKey: PublicKey;
    prevTxId: any;
    outputIndex: any;
    inputIndex: any;
    signature: any;
    sigtype: any;
    _checkObjectArgs(arg: any): void;
    /**
     * Serializes a transaction to a plain JS object
     * @return {Object}
     */
    toObject: () => any;
    toJSON(): any;
}
declare namespace TransactionSignature {
    /**
     * Builds a TransactionSignature from an object
     * @param {Object} object
     * @return {TransactionSignature}
     */
    function fromObject(object: any): TransactionSignature;
}
import PublicKey = require("../publickey");
//# sourceMappingURL=signature.d.ts.map