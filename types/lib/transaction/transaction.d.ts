/// <reference types="node" />
export = Transaction;
/**
 * Represents a transaction, a set of inputs and outputs to change ownership of tokens
 *
 * @param {*} serialized
 * @constructor
 */
declare function Transaction(serialized: any): Transaction;
declare class Transaction {
    /**
     * Represents a transaction, a set of inputs and outputs to change ownership of tokens
     *
     * @param {*} serialized
     * @constructor
     */
    constructor(serialized: any);
    version: number;
    inputs: any[];
    outputs: any[];
    _inputAmount: any;
    _outputAmount: number;
    readonly hash: any;
    readonly id: any;
    readonly idem: any;
    readonly inputAmount: any;
    readonly outputAmount: any;
    /**
     * Retrieve the little endian hash of the transaction (used for serialization)
     * @return {Buffer}
     */
    _getHash(): Buffer;
    _getIdem(): buffer.Buffer;
    _getSatisfier(): buffer.Buffer;
    _getIdHash(): buffer.Buffer;
    /**
     * Retrieve a hexa string that can be used with bitcoind's CLI interface
     * (decoderawtransaction, sendrawtransaction)
     *
     * @param {Object|boolean=} unsafe if true, skip all tests. if it's an object,
     *   it's expected to contain a set of flags to skip certain tests:
     * * `disableAll`: disable all checks
     * * `disableLargeFees`: disable checking for fees that are too large
     * * `disableIsFullySigned`: disable checking if all inputs are fully signed
     * * `disableDustOutputs`: disable checking if there are no outputs that are dust amounts
     * * `disableMoreOutputThanInput`: disable checking if the transaction spends more bitcoins than the sum of the input amounts
     * @return {string}
     */
    serialize(unsafe?: (any | boolean) | undefined): string;
    uncheckedSerialize: () => any;
    toString(): any;
    /**
     * Retrieve a hexa string that can be used with bitcoind's CLI interface
     * (decoderawtransaction, sendrawtransaction)
     *
     * @param {Object} opts allows to skip certain tests. {@see Transaction#serialize}
     * @return {string}
     */
    checkedSerialize(opts: any): string;
    invalidSatoshis(): boolean;
    /**
     * Retrieve a possible error that could appear when trying to serialize and
     * broadcast this transaction.
     *
     * @param {Object} opts allows to skip certain tests. {@see Transaction#serialize}
     * @return {nexcore.Error}
     */
    getSerializationError(opts: any): nexcore.Error;
    _hasFeeError(opts: any, unspent: any): any;
    _missingChange(): boolean;
    _hasDustOutputs(opts: any): any;
    _isMissingSignatures(opts: any): any;
    inspect(): string;
    toBuffer(): any;
    toBufferWriter(writer: any): any;
    toBufferIdem(): any;
    toBufferWriterIdem(writer: any): any;
    toBufferSatisfier(): any;
    toBufferWriterSatisfier(writer: any): any;
    fromBuffer(buffer: any): this;
    fromBufferReader(reader: any): this;
    nLockTime: any;
    toObject: () => {
        idem: any;
        hash: any;
        version: number;
        inputs: any[];
        outputs: any[];
        nLockTime: any;
    };
    toJSON(): {
        idem: any;
        hash: any;
        version: number;
        inputs: any[];
        outputs: any[];
        nLockTime: any;
    };
    fromObject(arg: any): this;
    _changeIndex: any;
    _changeScript: import("../script/script");
    _fee: any;
    _checkConsistency(arg: any): void;
    /**
     * Sets nLockTime so that transaction is not valid until the desired date(a
     * timestamp in seconds since UNIX epoch is also accepted)
     *
     * @param {Date | Number} time
     * @return {Transaction} this
     */
    lockUntilDate(time: Date | number): Transaction;
    /**
     * Sets nLockTime so that transaction is not valid until the desired block
     * height.
     *
     * @param {Number} height
     * @return {Transaction} this
     */
    lockUntilBlockHeight(height: number): Transaction;
    /**
     *  Returns a semantic version of the transaction's nLockTime.
     *  @return {Number|Date}
     *  If nLockTime is 0, it returns null,
     *  if it is < 500000000, it returns a block height (number)
     *  else it returns a Date object.
     */
    getLockTime(): number | Date;
    fromString(string: any): void;
    _newTransaction(): void;
    /**
     * @typedef {Object} Transaction~fromObject
     * @property {string} prevTxId
     * @property {number} outputIndex
     * @property {(Buffer|string|Script)} script
     * @property {number} satoshis
     */
    /**
     * Add an input to this transaction. This is a high level interface
     * to add an input, for more control, use @{link Transaction#addInput}.
     *
     * Can receive, as output information, the output of bitcoind's `listunspent` command,
     * and a slightly fancier format recognized by nexcore:
     *
     * ```
     * {
     *  address: 'mszYqVnqKoQx4jcTdJXxwKAissE3Jbrrc1',
     *  txId: 'a477af6b2667c29670467e4e0728b685ee07b240235771862318e29ddbe58458',
     *  outputIndex: 0,
     *  script: Script.empty(),
     *  satoshis: 1020000
     * }
     * ```
     * Where `address` can be either a string or a nexcore Address object. The
     * same is true for `script`, which can be a string or a nexcore Script.
     *
     * Beware that this resets all the signatures for inputs (in further versions,
     * SIGHASH_SINGLE or SIGHASH_NONE signatures will not be reset).
     *
     * @example
     * ```javascript
     * var transaction = new Transaction();
     *
     * // From a pay to public key hash output from bitcoind's listunspent
     * transaction.from({'txid': '0000...', vout: 0, amount: 0.1, scriptPubKey: 'OP_DUP ...'});
     *
     * // From a pay to public key hash output
     * transaction.from({'txId': '0000...', outputIndex: 0, satoshis: 1000, script: 'OP_DUP ...'});
     *
     * // From a multisig P2SH output
     * transaction.from({'txId': '0000...', inputIndex: 0, satoshis: 1000, script: '... OP_HASH'},
     *                  ['03000...', '02000...'], 2);
     * ```
     *
     * @param {(Array.<Transaction~fromObject>|Transaction~fromObject)} utxo
     * @param {Array=} pubkeys
     * @param {number=} threshold
     * @param {Object=} opts - Several options:
     *        - noSorting: defaults to false, if true and is multisig, don't
     *                      sort the given public keys before creating the script
     */
    from(utxo: any, pubkeys?: any[] | undefined, threshold?: number | undefined, opts?: any | undefined): this;
    /**
     * associateInputs - Update inputs with utxos, allowing you to specify value, and pubkey.
     * Populating these inputs allows for them to be signed with .sign(privKeys)
     *
     * @param {Array<Object>} utxos
     * @param {Array<string | PublicKey>} pubkeys
     * @param {number} threshold
     * @param {Object} opts
     * @returns {Array<number>}
     */
    associateInputs(utxos: Array<any>, pubkeys: Array<string | PublicKey>, threshold: number, opts: any): Array<number>;
    _selectInputType(utxo: any, pubkeys: any, threshold: any): typeof import("./input/publickey") | typeof import("./input/multisig") | typeof import("./input/multisigscripthash") | typeof import("./input/scripttemplate") | {
        (params: any): import("./input/input");
        new (params: any): import("./input/input");
        MAXINT: number;
        DEFAULT_SEQNUMBER: number;
        DEFAULT_LOCKTIME_SEQNUMBER: number;
        DEFAULT_RBF_SEQNUMBER: number;
        SEQUENCE_LOCKTIME_TYPE_FLAG: number;
        DEFAULT_TYPE: number;
        fromObject(obj: any): import("./input/input");
        fromBufferReader(br: any): import("./input/input");
        PublicKey: typeof import("./input/publickey");
        PublicKeyHash: typeof import("./input/publickeyhash");
        PublicKeyTemplate: typeof import("./input/publickeytemplate");
        MultiSig: typeof import("./input/multisig");
        MultiSigScriptHash: typeof import("./input/multisigscripthash");
        Escrow: typeof import("./input/escrow");
        ScriptTemplate: typeof import("./input/scripttemplate");
    };
    _getInputFrom(utxo: any, pubkeys: any, threshold: any, opts: any): import("./input/input") | import("./input/publickey") | import("./input/multisig") | import("./input/multisigscripthash") | import("./input/scripttemplate");
    _fromEscrowUtxo(utxo: any, pubkeys: any): void;
    _fromPubKeyUtxo(utxo: any): void;
    _fromScriptTemplateUtxo(utxo: any, templateScript: any, constraintScript: any, visibleArgs: any, pubKey: any): void;
    _fromMultisigUtxo(utxo: any, pubkeys: any, threshold: any, opts: any): void;
    /**
     * Add an input to this transaction. The input must be an instance of the `Input` class.
     * It should have information about the Output that it's spending, but if it's not already
     * set, two additional parameters, `outputScript` and `satoshis` can be provided.
     *
     * @param {Input} input
     * @param {String|Script} outputScript
     * @param {number} satoshis
     * @return Transaction this, for chaining
     */
    addInput(input: {
        (params: any): import("./input/input");
        new (params: any): import("./input/input");
        MAXINT: number;
        DEFAULT_SEQNUMBER: number;
        DEFAULT_LOCKTIME_SEQNUMBER: number;
        DEFAULT_RBF_SEQNUMBER: number;
        SEQUENCE_LOCKTIME_TYPE_FLAG: number;
        DEFAULT_TYPE: number;
        fromObject(obj: any): import("./input/input");
        fromBufferReader(br: any): import("./input/input");
        PublicKey: typeof import("./input/publickey");
        PublicKeyHash: typeof import("./input/publickeyhash");
        PublicKeyTemplate: typeof import("./input/publickeytemplate");
        MultiSig: typeof import("./input/multisig");
        MultiSigScriptHash: typeof import("./input/multisigscripthash");
        Escrow: typeof import("./input/escrow");
        ScriptTemplate: typeof import("./input/scripttemplate");
    }, outputScript: string | {
        (from?: any): import("../script/script");
        new (from?: any): import("../script/script");
        fromBuffer(buffer: any): import("../script/script");
        fromASM(str: any): import("../script/script");
        fromHex(str: any): import("../script/script");
        fromString(str: any): import("../script/script");
        types: typeof import("../script/script").types;
        OP_RETURN_STANDARD_SIZE: number;
        outputIdentifiers: typeof import("../script/script").outputIdentifiers;
        inputIdentifiers: typeof import("../script/script").inputIdentifiers;
        buildEscrowOut(inputPublicKeys: PublicKey[], reclaimPublicKey: PublicKey): import("../script/script");
        buildMultisigOut(publicKeys: PublicKey[], threshold: number, opts?: any): import("../script/script");
        buildMultisigIn(pubkeys: PublicKey[], threshold: number, signatures: any[], opts?: any): import("../script/script");
        buildP2SHMultisigIn(pubkeys: PublicKey[], threshold: number, signatures: any[], opts?: any): import("../script/script");
        buildPublicKeyHashOut(to: PublicKey | Address): import("../script/script");
        buildScriptTemplateOut(to: string | PublicKey | Address): import("../script/script");
        buildGroupedPublicKeyTemplateOut(to: string | PublicKey | Address, groupId: buffer.Buffer, groupAmount: buffer.Buffer): import("../script/script");
        buildPublicKeyOut(pubkey: any): import("../script/script");
        buildDataOut(data: string | buffer.Buffer, encoding: string): import("../script/script");
        buildScriptHashOut(script: Address | import("../script/script")): import("../script/script");
        buildPublicKeyIn(signature: buffer.Buffer | Signature, sigtype?: number): import("../script/script");
        buildPublicKeyHashIn(publicKey: string | buffer.Buffer | PublicKey, signature: buffer.Buffer | Signature, sigtype?: number): import("../script/script");
        buildPublicKeyTemplateIn(publicKey: string | buffer.Buffer | PublicKey, signature: buffer.Buffer | Signature, sigtype?: number): import("../script/script");
        buildScriptTemplateIn(template: import("../script/script"), constraint: Opcode | import("../script/script"), satisfier: buffer.Buffer, sigtype?: number): import("../script/script");
        buildEscrowIn(publicKey: PublicKey, signature: Signature, redeemScript: RedeemScript): import("../script/script");
        empty(): import("../script/script");
        fromAddress(address: any): import("../script/script");
        Interpreter: typeof import("../script/interpreter");
    }, satoshis: number): this;
    /**
     * Add an input to this transaction, without checking that the input has information about
     * the output that it's spending.
     *
     * @param {Input} input
     * @return Transaction this, for chaining
     */
    uncheckedAddInput(input: {
        (params: any): import("./input/input");
        new (params: any): import("./input/input");
        MAXINT: number;
        DEFAULT_SEQNUMBER: number;
        DEFAULT_LOCKTIME_SEQNUMBER: number;
        DEFAULT_RBF_SEQNUMBER: number;
        SEQUENCE_LOCKTIME_TYPE_FLAG: number;
        DEFAULT_TYPE: number;
        fromObject(obj: any): import("./input/input");
        fromBufferReader(br: any): import("./input/input");
        PublicKey: typeof import("./input/publickey");
        PublicKeyHash: typeof import("./input/publickeyhash");
        PublicKeyTemplate: typeof import("./input/publickeytemplate");
        MultiSig: typeof import("./input/multisig");
        MultiSigScriptHash: typeof import("./input/multisigscripthash");
        Escrow: typeof import("./input/escrow");
        ScriptTemplate: typeof import("./input/scripttemplate");
    }): this;
    /**
     * Returns true if the transaction has enough info on all inputs to be correctly validated
     *
     * @return {boolean}
     */
    hasAllUtxoInfo(): boolean;
    /**
     * Manually set the fee for this transaction. Beware that this resets all the signatures
     * for inputs (in further versions, SIGHASH_SINGLE or SIGHASH_NONE signatures will not
     * be reset).
     *
     * @param {number} amount satoshis to be sent
     * @return {Transaction} this, for chaining
     */
    fee(amount: number): Transaction;
    /**
     * Manually set the fee per KB for this transaction. Beware that this resets all the signatures
     * for inputs (in further versions, SIGHASH_SINGLE or SIGHASH_NONE signatures will not
     * be reset).
     *
     * @param {number} amount satoshis per KB to be sent
     * @return {Transaction} this, for chaining
     */
    feePerKb(amount: number): Transaction;
    _feePerKb: number;
    /**
     * Manually set the fee per Byte for this transaction. Beware that this resets all the signatures
     * for inputs (in further versions, SIGHASH_SINGLE or SIGHASH_NONE signatures will not
     * be reset).
     * fee per Byte will be ignored if fee per KB is set
     *
     * @param {number} amount satoshis per Byte to be sent
     * @return {Transaction} this, for chaining
     */
    feePerByte(amount: number): Transaction;
    _feePerByte: number;
    /**
     * Set the change address for this transaction
     *
     * Beware that this resets all the signatures for inputs (in further versions,
     * SIGHASH_SINGLE or SIGHASH_NONE signatures will not be reset).
     *
     * @param {Address|string} address An address for change to be sent to.
     * @return {Transaction} this, for chaining
     */
    change(address: Address | string): Transaction;
    /**
     * Set the Zero-Confirmation Escrow (ZCE) address for this transaction
     *
     * @param {Address} address The Zero-Confirmation Escrow (ZCE) address for this payment
     * @param {number} amount The amount in satoshis to send to the ZCE address
     * @return {Transaction} this, for chaining
     */
    escrow(address: Address, amount: number): Transaction;
    /**
     * @return {Output} change output, if it exists
     */
    getChangeOutput(): Output;
    /**
     * @typedef {Object} Transaction~toObject
     * @property {(string|Address)} address
     * @property {number} satoshis
     */
    /**
     * Add an output to the transaction.
     *
     * Beware that this resets all the signatures for inputs.
     *
     * @param {(string|Address|Array.<Transaction~toObject>)} address
     * @param {number} amount in satoshis
     * @return {Transaction} this, for chaining
     */
    to(address: any, amount: number, outType: any): Transaction;
    /**
     * Add an grouped output to the transaction.
     *
     * Beware that this resets all the signatures for inputs.
     *
     * @param {string|Address|Array<{address: string | Address, groupId: Buffer, amount: Buffer}>} address - must be p2pkt
     * @param {Buffer} groupId group id
     * @param {Buffer} amount group amount
     * @return {Transaction} this, for chaining
     */
    toGrouped(address: string | Address | Array<{
        address: string | Address;
        groupId: Buffer;
        amount: Buffer;
    }>, groupId: Buffer, amount: Buffer): Transaction;
    /**
     * Add an OP_RETURN output to the transaction.
     *
     * Beware that this resets all the signatures for inputs (in further versions,
     * SIGHASH_SINGLE or SIGHASH_NONE signatures will not be reset).
     *
     * @param {Buffer|string} value the data to be stored in the OP_RETURN output.
     *    In case of a string, the UTF-8 representation will be stored
     * @return {Transaction} this, for chaining
     */
    addData(value: Buffer | string): Transaction;
    /**
     * Add an Group Token description OP_RETURN output to the transaction.
     *
     * @param script the script to be stored as OP_RETURN output.
     * @return {Transaction} this, for chaining
     */
    addGroupData(script: any): Transaction;
    /**
     * Add an output to the transaction.
     *
     * @param {Output} output the output to add.
     * @return {Transaction} this, for chaining
     */
    addOutput(output: Output): Transaction;
    /**
     * Remove all outputs from the transaction.
     *
     * @return {Transaction} this, for chaining
     */
    clearOutputs(): Transaction;
    _addOutput(output: any): void;
    /**
     * Calculates or gets the total output amount in satoshis
     *
     * @return {Number} the transaction total output amount
     */
    _getOutputAmount(): number;
    /**
     * Calculates or gets the total input amount in satoshis
     *
     * @return {Number} the transaction total input amount
     */
    _getInputAmount(): number;
    _updateChangeOutput(): void;
    /**
     * Calculates the fee of the transaction.
     *
     * If there's a fixed fee set, return that.
     *
     * If there is no change output set, the fee is the
     * total value of the outputs minus inputs. Note that
     * a serialized transaction only specifies the value
     * of its outputs. (The value of inputs are recorded
     * in the previous transaction outputs being spent.)
     * This method therefore raises a "MissingPreviousOutput"
     * error when called on a serialized transaction.
     *
     * If there's no fee set and no change address,
     * estimate the fee based on size.
     *
     * @return {Number} fee of this transaction in satoshis
     */
    getFee(): number;
    /**
     * Estimates fee from serialized transaction size in bytes.
     */
    _estimateFee(): number;
    _getUnspentValue(): number;
    _clearSignatures(): void;
    _estimateSize(): number;
    _removeOutput(index: any): void;
    removeOutput(index: any): void;
    /**
     * Sort a transaction's inputs and outputs according to BIP69
     *
     * @see {https://github.com/bitcoin/bips/blob/master/bip-0069.mediawiki}
     * @return {Transaction} this
     */
    sort(): Transaction;
    /**
     * Randomize this transaction's outputs ordering. The shuffling algorithm is a
     * version of the Fisher-Yates shuffle, provided by lodash's _.shuffle().
     *
     * @return {Transaction} this
     */
    shuffleOutputs(): Transaction;
    /**
     * Sort this transaction's outputs, according to a given sorting function that
     * takes an array as argument and returns a new array, with the same elements
     * but with a different order. The argument function MUST NOT modify the order
     * of the original array
     *
     * @param {Function} sortingFunction
     * @return {Transaction} this
     */
    sortOutputs(sortingFunction: Function): Transaction;
    /**
     * Sort this transaction's inputs, according to a given sorting function that
     * takes an array as argument and returns a new array, with the same elements
     * but with a different order.
     *
     * @param {Function} sortingFunction
     * @return {Transaction} this
     */
    sortInputs(sortingFunction: Function): Transaction;
    _newOutputOrder(newOutputs: any): this;
    removeInput(txId: any, outputIndex: any): void;
    /**
     * Sign the transaction using one or more private keys.
     *
     * It tries to sign each input, verifying that the signature will be valid
     * (matches a public key).
     *
     * @param {Array|String|PrivateKey} privateKey
     * @param {number} sigtype
     * @return {Transaction} this, for chaining
     */
    sign(privateKey: any[] | string | PrivateKey, sigtype: number, signingMethod: any): Transaction;
    getSignatures(privKey: any, sigtype: any, signingMethod: any): any[];
    /**
     * Add a signature to the transaction
     *
     * @param {Object} signature
     * @param {number} signature.inputIndex
     * @param {number} signature.sigtype
     * @param {PublicKey} signature.publicKey
     * @param {Signature} signature.signature
     * @param {String} signingMethod "ecdsa" or "schnorr"
     * @return {Transaction} this, for chaining
     */
    applySignature(signature: {
        inputIndex: number;
        sigtype: number;
        publicKey: PublicKey;
        signature: Signature;
    }, signingMethod: string): Transaction;
    isFullySigned(): any;
    isValidSignature(signature: any): any;
    /**
     * @returns {bool} whether the signature is valid for this transaction input
     */
    verifySignature(sig: any, pubkey: any, nin: any, subscript: any, satoshisBN: any, flags: any, signingMethod: any): bool;
    /**
     * Check that a transaction passes basic sanity tests. If not, return a string
     * describing the error. This function contains the same logic as
     * CheckTransaction in bitcoin core.
     */
    verify(): string | true;
    isZceSecured(escrowReclaimTx: any, instantAcceptanceEscrow: any, requiredFeeRate: any): boolean;
    /**
     * Analogous to nexad's IsCoinBase function in transaction.h
     */
    isCoinbase(): any;
    setVersion(version: any): this;
}
declare namespace Transaction {
    export { DUST_AMOUNT, FEE_SECURITY_MARGIN, MAX_MONEY, NLOCKTIME_BLOCKHEIGHT_LIMIT, NLOCKTIME_MAX_VALUE, FEE_PER_KB, CHANGE_OUTPUT_MAX_SIZE, shallowCopy, Transaction };
}
import buffer = require("buffer");
import PublicKey = require("../publickey");
import Address = require("../address");
import Signature = require("../crypto/signature");
import Opcode = require("../opcode");
import Output = require("./output");
import PrivateKey = require("../privatekey");
declare var DUST_AMOUNT: number;
declare var FEE_SECURITY_MARGIN: number;
declare var MAX_MONEY: number;
declare var NLOCKTIME_BLOCKHEIGHT_LIMIT: number;
declare var NLOCKTIME_MAX_VALUE: number;
declare var FEE_PER_KB: number;
declare var CHANGE_OUTPUT_MAX_SIZE: number;
/**
 * Create a 'shallow' copy of the transaction, by serializing and deserializing
 * it dropping any additional information that inputs and outputs may have hold
 *
 * @param {Transaction} transaction
 * @return {Transaction}
 */
declare function shallowCopy(transaction: Transaction): Transaction;
/**
 * ~fromObject
 */
type Transaction = {
    prevTxId: string;
    outputIndex: number;
    script: (Buffer | string | {
        (from?: any): import("../script/script");
        new (from?: any): import("../script/script");
        fromBuffer(buffer: any): import("../script/script");
        fromASM(str: any): import("../script/script");
        fromHex(str: any): import("../script/script");
        fromString(str: any): import("../script/script");
        types: typeof import("../script/script").types;
        OP_RETURN_STANDARD_SIZE: number;
        outputIdentifiers: typeof import("../script/script").outputIdentifiers;
        inputIdentifiers: typeof import("../script/script").inputIdentifiers;
        buildEscrowOut(inputPublicKeys: PublicKey[], reclaimPublicKey: PublicKey): import("../script/script");
        buildMultisigOut(publicKeys: PublicKey[], threshold: number, opts?: any): import("../script/script");
        buildMultisigIn(pubkeys: PublicKey[], threshold: number, signatures: any[], opts?: any): import("../script/script");
        buildP2SHMultisigIn(pubkeys: PublicKey[], threshold: number, signatures: any[], opts?: any): import("../script/script");
        buildPublicKeyHashOut(to: PublicKey | Address): import("../script/script");
        buildScriptTemplateOut(to: string | PublicKey | Address): import("../script/script");
        buildGroupedPublicKeyTemplateOut(to: string | PublicKey | Address, groupId: buffer.Buffer, groupAmount: buffer.Buffer): import("../script/script");
        buildPublicKeyOut(pubkey: any): import("../script/script");
        buildDataOut(data: string | buffer.Buffer, encoding: string): import("../script/script");
        buildScriptHashOut(script: Address | import("../script/script")): import("../script/script");
        buildPublicKeyIn(signature: buffer.Buffer | Signature, sigtype?: number): import("../script/script");
        buildPublicKeyHashIn(publicKey: string | buffer.Buffer | PublicKey, signature: buffer.Buffer | Signature, sigtype?: number): import("../script/script");
        buildPublicKeyTemplateIn(publicKey: string | buffer.Buffer | PublicKey, signature: buffer.Buffer | Signature, sigtype?: number): import("../script/script");
        buildScriptTemplateIn(template: import("../script/script"), constraint: Opcode | import("../script/script"), satisfier: buffer.Buffer, sigtype?: number): import("../script/script");
        buildEscrowIn(publicKey: PublicKey, signature: Signature, redeemScript: RedeemScript): import("../script/script");
        empty(): import("../script/script");
        fromAddress(address: any): import("../script/script");
        Interpreter: typeof import("../script/interpreter");
    });
    satoshis: number;
};
//# sourceMappingURL=transaction.d.ts.map