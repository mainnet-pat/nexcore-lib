/// <reference types="node" />
/**
 * Returns a buffer of length 32 bytes with the hash that needs to be signed
 * for OP_CHECKSIG.
 *
 * @name Signing.sighash
 * @param {Transaction} transaction the transaction to sign
 * @param {number} sighashType the type of the hash
 * @param {number} inputNumber the input index for the signature
 * @param {Script} subscript the script that will be signed
 * @param {satoshisBN} input's amount (for  ForkId signatures)
 *
 */
export function sighash(transaction: import("./transaction"), sighashType: number, inputNumber: number, subscript: {
    (from?: any): import("../script/script");
    new (from?: any): import("../script/script");
    fromBuffer(buffer: any): import("../script/script");
    fromASM(str: any): import("../script/script");
    fromHex(str: any): import("../script/script");
    fromString(str: any): import("../script/script");
    types: typeof import("../script/script").types;
    OP_RETURN_STANDARD_SIZE: number;
    outputIdentifiers: typeof import("../script/script").outputIdentifiers;
    inputIdentifiers: typeof import("../script/script").inputIdentifiers;
    buildEscrowOut(inputPublicKeys: import("../publickey")[], reclaimPublicKey: import("../publickey")): import("../script/script");
    buildMultisigOut(publicKeys: import("../publickey")[], threshold: number, opts?: any): import("../script/script");
    buildMultisigIn(pubkeys: import("../publickey")[], threshold: number, signatures: any[], opts?: any): import("../script/script");
    buildP2SHMultisigIn(pubkeys: import("../publickey")[], threshold: number, signatures: any[], opts?: any): import("../script/script");
    buildPublicKeyHashOut(to: import("../publickey") | import("../address")): import("../script/script");
    buildScriptTemplateOut(to: string | import("../publickey") | import("../address")): import("../script/script");
    buildGroupedPublicKeyTemplateOut(to: string | import("../publickey") | import("../address"), groupId: buffer.Buffer, groupAmount: buffer.Buffer): import("../script/script");
    buildPublicKeyOut(pubkey: any): import("../script/script");
    buildDataOut(data: string | buffer.Buffer, encoding: string): import("../script/script");
    buildScriptHashOut(script: import("../address") | import("../script/script")): import("../script/script");
    buildPublicKeyIn(signature: buffer.Buffer | Signature, sigtype?: number): import("../script/script");
    buildPublicKeyHashIn(publicKey: string | buffer.Buffer | import("../publickey"), signature: buffer.Buffer | Signature, sigtype?: number): import("../script/script");
    buildPublicKeyTemplateIn(publicKey: string | buffer.Buffer | import("../publickey"), signature: buffer.Buffer | Signature, sigtype?: number): import("../script/script");
    buildScriptTemplateIn(template: import("../script/script"), constraint: Opcode | import("../script/script"), satisfier: buffer.Buffer, sigtype?: number): import("../script/script");
    buildEscrowIn(publicKey: import("../publickey"), signature: Signature, redeemScript: RedeemScript): import("../script/script");
    empty(): import("../script/script");
    fromAddress(address: any): import("../script/script");
    Interpreter: typeof Interpreter;
}, satoshisBN: any, flags: any): buffer.Buffer;
/**
 * Create a signature
 *
 * @name Signing.sign
 * @param {Transaction} transaction
 * @param {PrivateKey} privateKey
 * @param {number} sighash
 * @param {number} inputIndex
 * @param {Script} subscript
 * @param {satoshisBN} input's amount
 * @param {signingMethod} signingMethod "ecdsa" or "schnorr" to sign a tx
 * @return {Signature}
 */
export function sign(transaction: Transaction, privateKey: PrivateKey, sighashType: any, inputIndex: number, subscript: {
    (from?: any): import("../script/script");
    new (from?: any): import("../script/script");
    fromBuffer(buffer: any): import("../script/script");
    fromASM(str: any): import("../script/script");
    fromHex(str: any): import("../script/script");
    fromString(str: any): import("../script/script");
    types: typeof import("../script/script").types;
    OP_RETURN_STANDARD_SIZE: number;
    outputIdentifiers: typeof import("../script/script").outputIdentifiers;
    inputIdentifiers: typeof import("../script/script").inputIdentifiers;
    buildEscrowOut(inputPublicKeys: import("../publickey")[], reclaimPublicKey: import("../publickey")): import("../script/script");
    buildMultisigOut(publicKeys: import("../publickey")[], threshold: number, opts?: any): import("../script/script");
    buildMultisigIn(pubkeys: import("../publickey")[], threshold: number, signatures: any[], opts?: any): import("../script/script");
    buildP2SHMultisigIn(pubkeys: import("../publickey")[], threshold: number, signatures: any[], opts?: any): import("../script/script");
    buildPublicKeyHashOut(to: import("../publickey") | import("../address")): import("../script/script");
    buildScriptTemplateOut(to: string | import("../publickey") | import("../address")): import("../script/script");
    buildGroupedPublicKeyTemplateOut(to: string | import("../publickey") | import("../address"), groupId: buffer.Buffer, groupAmount: buffer.Buffer): import("../script/script");
    buildPublicKeyOut(pubkey: any): import("../script/script");
    buildDataOut(data: string | buffer.Buffer, encoding: string): import("../script/script");
    buildScriptHashOut(script: import("../address") | import("../script/script")): import("../script/script");
    buildPublicKeyIn(signature: buffer.Buffer | Signature, sigtype?: number): import("../script/script");
    buildPublicKeyHashIn(publicKey: string | buffer.Buffer | import("../publickey"), signature: buffer.Buffer | Signature, sigtype?: number): import("../script/script");
    buildPublicKeyTemplateIn(publicKey: string | buffer.Buffer | import("../publickey"), signature: buffer.Buffer | Signature, sigtype?: number): import("../script/script");
    buildScriptTemplateIn(template: import("../script/script"), constraint: Opcode | import("../script/script"), satisfier: buffer.Buffer, sigtype?: number): import("../script/script");
    buildEscrowIn(publicKey: import("../publickey"), signature: Signature, redeemScript: RedeemScript): import("../script/script");
    empty(): import("../script/script");
    fromAddress(address: any): import("../script/script");
    Interpreter: typeof Interpreter;
}, satoshisBN: any, flags: any, signingMethod: any): Signature;
/**
 * Verify a signature
 *
 * @name Signing.verify
 * @param {Transaction} transaction
 * @param {Signature} signature
 * @param {PublicKey} publicKey
 * @param {number} inputIndex
 * @param {Script} subscript
 * @param {satoshisBN} input's amount
 * @param {flags} verification flags
 * @param {signingMethod} signingMethod 'ecdsa' or 'schnorr' to sign a tx
 * @return {boolean}
 */
export function verify(transaction: Transaction, signature: Signature, publicKey: PublicKey, inputIndex: number, subscript: {
    (from?: any): import("../script/script");
    new (from?: any): import("../script/script");
    fromBuffer(buffer: any): import("../script/script");
    fromASM(str: any): import("../script/script");
    fromHex(str: any): import("../script/script");
    fromString(str: any): import("../script/script");
    types: typeof import("../script/script").types;
    OP_RETURN_STANDARD_SIZE: number;
    outputIdentifiers: typeof import("../script/script").outputIdentifiers;
    inputIdentifiers: typeof import("../script/script").inputIdentifiers;
    buildEscrowOut(inputPublicKeys: import("../publickey")[], reclaimPublicKey: import("../publickey")): import("../script/script");
    buildMultisigOut(publicKeys: import("../publickey")[], threshold: number, opts?: any): import("../script/script");
    buildMultisigIn(pubkeys: import("../publickey")[], threshold: number, signatures: any[], opts?: any): import("../script/script");
    buildP2SHMultisigIn(pubkeys: import("../publickey")[], threshold: number, signatures: any[], opts?: any): import("../script/script");
    buildPublicKeyHashOut(to: import("../publickey") | import("../address")): import("../script/script");
    buildScriptTemplateOut(to: string | import("../publickey") | import("../address")): import("../script/script");
    buildGroupedPublicKeyTemplateOut(to: string | import("../publickey") | import("../address"), groupId: buffer.Buffer, groupAmount: buffer.Buffer): import("../script/script");
    buildPublicKeyOut(pubkey: any): import("../script/script");
    buildDataOut(data: string | buffer.Buffer, encoding: string): import("../script/script");
    buildScriptHashOut(script: import("../address") | import("../script/script")): import("../script/script");
    buildPublicKeyIn(signature: buffer.Buffer | Signature, sigtype?: number): import("../script/script");
    buildPublicKeyHashIn(publicKey: string | buffer.Buffer | import("../publickey"), signature: buffer.Buffer | Signature, sigtype?: number): import("../script/script");
    buildPublicKeyTemplateIn(publicKey: string | buffer.Buffer | import("../publickey"), signature: buffer.Buffer | Signature, sigtype?: number): import("../script/script");
    buildScriptTemplateIn(template: import("../script/script"), constraint: Opcode | import("../script/script"), satisfier: buffer.Buffer, sigtype?: number): import("../script/script");
    buildEscrowIn(publicKey: import("../publickey"), signature: Signature, redeemScript: RedeemScript): import("../script/script");
    empty(): import("../script/script");
    fromAddress(address: any): import("../script/script");
    Interpreter: typeof Interpreter;
}, satoshisBN: any, flags: any, signingMethod: any): boolean;
import buffer = require("buffer");
import Signature = require("../crypto/signature");
import Opcode = require("../opcode");
import Interpreter = require("../script/interpreter");
//# sourceMappingURL=sighash.d.ts.map