export = MultiSigScriptHashInput;
/**
 * @constructor
 */
declare function MultiSigScriptHashInput(input: any, pubkeys: any, threshold: any, signatures: any, opts: any, ...args: any[]): void;
declare class MultiSigScriptHashInput {
    /**
     * @constructor
     */
    constructor(input: any, pubkeys: any, threshold: any, signatures: any, opts: any, ...args: any[]);
    publicKeys: any;
    redeemScript: import("../../script/script");
    publicKeyIndex: {};
    threshold: any;
    signatures: any;
    checkBitsField: Uint8Array;
    toObject(...args: any[]): any;
    _deserializeSignatures(signatures: any): any;
    _serializeSignatures(): any;
    getSignatures(transaction: any, privateKey: any, index: any, sigtype: any, hashData: any, signingMethod: any): TransactionSignature[];
    addSignature(transaction: any, signature: any, signingMethod: any): this;
    _updateScript(signingMethod: any, checkBitsField: any): this;
    _createSignatures(signingMethod: any): any;
    clearSignatures(): void;
    isFullySigned(): boolean;
    countMissingSignatures(): number;
    countSignatures(): any;
    publicKeysWithoutSignature(): any;
    isValidSignature(transaction: any, signature: any, signingMethod: any): boolean;
    _estimateSize(): number;
}
declare namespace MultiSigScriptHashInput {
    let OPCODES_SIZE: number;
    let SIGNATURE_SIZE: number;
    let PUBKEY_SIZE: number;
}
import TransactionSignature = require("../signature");
//# sourceMappingURL=multisigscripthash.d.ts.map