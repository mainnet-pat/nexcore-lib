export = EscrowInput;
/**
 * @constructor
 */
declare function EscrowInput(input: any, inputPublicKeys: any, reclaimPublicKey: any, signatures: any, ...args: any[]): void;
declare class EscrowInput {
    /**
     * @constructor
     */
    constructor(input: any, inputPublicKeys: any, reclaimPublicKey: any, signatures: any, ...args: any[]);
    inputPublicKeys: any;
    reclaimPublicKey: any;
    redeemScript: import("../../script/script");
    signatures: any;
    getSignatures(transaction: any, privateKey: any, index: any, sigtype: any, hashData: any, signingMethod: any): TransactionSignature[];
    addSignature(transaction: any, signature: any, signingMethod: any): void;
    isValidSignature(transaction: any, signature: any, signingMethod: any): boolean;
    clearSignatures(): void;
    isFullySigned(): boolean;
    _deserializeSignatures(signatures: any): any;
}
import TransactionSignature = require("../signature");
//# sourceMappingURL=escrow.d.ts.map