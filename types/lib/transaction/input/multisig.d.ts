export = MultiSigInput;
/**
 * @constructor
 */
declare function MultiSigInput(input: any, pubkeys: any, threshold: any, signatures: any, opts: any, ...args: any[]): void;
declare class MultiSigInput {
    /**
     * @constructor
     */
    constructor(input: any, pubkeys: any, threshold: any, signatures: any, opts: any, ...args: any[]);
    publicKeys: any;
    publicKeyIndex: {};
    threshold: any;
    signatures: any;
    toObject(...args: any[]): any;
    _deserializeSignatures(signatures: any): any;
    _serializeSignatures(): any;
    getSignatures(transaction: any, privateKey: any, index: any, sigtype: any, hashData: any, signingMethod: any): any[];
    addSignature(transaction: any, signature: any, signingMethod: any): this;
    _updateScript(signingMethod: any): this;
    _createSignatures(signingMethod: any): any;
    clearSignatures(): void;
    isFullySigned(): boolean;
    countMissingSignatures(): number;
    countSignatures(): any;
    publicKeysWithoutSignature(): any;
    isValidSignature(transaction: any, signature: any, signingMethod: any): boolean;
    _estimateSize(): number;
}
declare namespace MultiSigInput {
    /**
     *
     * @param {Buffer[]} signatures
     * @param {PublicKey[]} publicKeys
     * @param {Transaction} transaction
     * @param {Integer} inputIndex
     * @param {Input} input
     * @returns {TransactionSignature[]}
     */
    function normalizeSignatures(transaction: Transaction, input: Input, inputIndex: Integer, signatures: Buffer[], publicKeys: PublicKey[], signingMethod: any): TransactionSignature[];
    let OPCODES_SIZE: number;
    let SIGNATURE_SIZE: number;
}
import Transaction = require("../transaction");
import Input = require("./input");
import PublicKey = require("../../publickey");
import TransactionSignature = require("../signature");
//# sourceMappingURL=multisig.d.ts.map