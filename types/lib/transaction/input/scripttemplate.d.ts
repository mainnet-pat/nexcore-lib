export = ScriptTemplateInput;
/**
 * Represents a special kind of input of not well-known PayToScriptTemplate kind.
 * @constructor
 */
declare function ScriptTemplateInput(input: any, templateScript: any, constraintScript: any, visibleArgs: any, pulicKey: any, ...args: any[]): void;
declare class ScriptTemplateInput {
    /**
     * Represents a special kind of input of not well-known PayToScriptTemplate kind.
     * @constructor
     */
    constructor(input: any, templateScript: any, constraintScript: any, visibleArgs: any, pulicKey: any, ...args: any[]);
    visibleArgs: any;
    publicKey: any;
    templateScript: any;
    constraintScript: any;
    toObject(...args: any[]): any;
    /**
     * @param {Transaction} transaction - the transaction to be signed
     * @param {PrivateKey} privateKey - the private key with which to sign the transaction
     * @param {number} index - the index of the input in the transaction input vector
     * @param {number=} sigtype - the type of signature, defaults to Signature.SIGHASH_ALL
     * @param {Buffer=} hashData - the precalculated hash of the public key associated with the privateKey provided
     * @param {String} signingMethod - the signing method used to sign tx "ecdsa" or "schnorr"
     * @return {Array} of objects that can be
     */
    getSignatures(transaction: Transaction, privateKey: PrivateKey, index: number, sigtype?: number | undefined, hashData?: Buffer | undefined, signingMethod: string): any[];
    /**
     * Add the provided signature
     *
     * @param {Object} signature
     * @param {PublicKey} signature.publicKey
     * @param {Signature} signature.signature
     * @param {number=} signature.sigtype
     * @param {String} signingMethod only "schnorr" allowed
     * @return {ScriptTemplateInput} this, for chaining
     */
    addSignature(transaction: any, signature: {
        publicKey: PublicKey;
        signature: Signature;
        sigtype?: number | undefined;
    }, signingMethod: string): ScriptTemplateInput;
    isValidSignature(transaction: any, signature: any, signingMethod: any): boolean;
    /**
     * Clear the input's signature
     * @return {ScriptTemplateInput} this, for chaining
     */
    clearSignatures(): ScriptTemplateInput;
    /**
     * Query whether the input is signed
     * @return {boolean}
     */
    isFullySigned(): boolean;
    _estimateSize(): number;
    _estimateScriptSize(): number;
}
import Signature = require("../../crypto/signature");
//# sourceMappingURL=scripttemplate.d.ts.map