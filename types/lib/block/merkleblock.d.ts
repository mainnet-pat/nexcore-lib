export = MerkleBlock;
/**
 * Instantiate a MerkleBlock from a Buffer, JSON object, or Object with
 * the properties of the Block
 *
 * @param {*} - A Buffer, JSON string, or Object representing a MerkleBlock
 * @returns {MerkleBlock}
 * @constructor
 */
declare function MerkleBlock(arg: any): MerkleBlock;
declare class MerkleBlock {
    /**
     * Instantiate a MerkleBlock from a Buffer, JSON object, or Object with
     * the properties of the Block
     *
     * @param {*} - A Buffer, JSON string, or Object representing a MerkleBlock
     * @returns {MerkleBlock}
     * @constructor
     */
    constructor(arg: any);
    _flagBitsUsed: number;
    _hashesUsed: number;
    /**
     * @returns {Buffer} - A buffer of the block
     */
    toBuffer(): Buffer;
    /**
     * @param {BufferWriter} - An existing instance of BufferWriter
     * @returns {BufferWriter} - An instance of BufferWriter representation of the MerkleBlock
     */
    toBufferWriter(bw: any): BufferWriter;
    /**
     * @returns {Object} - A plain object with the MerkleBlock properties
     */
    toObject: () => any;
    toJSON(): any;
    /**
     * Verify that the MerkleBlock is valid
     * @returns {Boolean} - True/False whether this MerkleBlock is Valid
     */
    validMerkleTree(): boolean;
    /**
     * Return a list of all the txs hash that match the filter
     * @returns {Array} - txs hash that match the filter
     */
    filterdTxsHash(): any[];
    private _traverseMerkleTree;
    private _calcTreeWidth;
    private _calcTreeHeight;
    private hasTransaction;
}
declare namespace MerkleBlock {
    /**
     * @param {Buffer} - MerkleBlock data in a Buffer object
     * @returns {MerkleBlock} - A MerkleBlock object
     */
    function fromBuffer(buf: any): MerkleBlock;
    /**
     * @param {BufferReader} - MerkleBlock data in a BufferReader object
     * @returns {MerkleBlock} - A MerkleBlock object
     */
    function fromBufferReader(br: any): MerkleBlock;
    /**
     * @param {Buffer} - MerkleBlock data
     * @returns {Object} - An Object representing merkleblock data
     * @private
     */
    function _fromBufferReader(br: any): any;
    /**
     * @param {Object} - A plain JavaScript object
     * @returns {Block} - An instance of block
     */
    function fromObject(obj: any): Block;
}
import BufferWriter = require("../encoding/bufferwriter");
//# sourceMappingURL=merkleblock.d.ts.map