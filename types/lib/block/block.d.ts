export = Block;
/**
 * Instantiate a Block from a Buffer, JSON object, or Object with
 * the properties of the Block
 *
 * @param {*} - A Buffer, JSON string, or Object
 * @returns {Block}
 * @constructor
 */
declare function Block(arg: any): Block;
declare class Block {
    /**
     * Instantiate a Block from a Buffer, JSON object, or Object with
     * the properties of the Block
     *
     * @param {*} - A Buffer, JSON string, or Object
     * @returns {Block}
     * @constructor
     */
    constructor(arg: any);
    /**
     * @returns {Object} - A plain object with the block properties
     */
    toObject: () => any;
    toJSON(): any;
    /**
     * @returns {Buffer} - A buffer of the block
     */
    toBuffer(): Buffer;
    /**
     * @returns {string} - A hex encoded string of the block
     */
    toString(): string;
    /**
     * @param {BufferWriter} - An existing instance of BufferWriter
     * @returns {BufferWriter} - An instance of BufferWriter representation of the Block
     */
    toBufferWriter(bw: any): BufferWriter;
    /**
     * Will iterate through each transaction and return an array of hashes
     * @returns {Array} - An array with transaction hashes
     */
    getTransactionHashes(): any[];
    /**
     * Will build a merkle tree of all the transactions, ultimately arriving at
     * a single point, the merkle root.
     * @link https://en.bitcoin.it/wiki/Protocol_specification#Merkle_Trees
     * @returns {Array} - An array with each level of the tree after the other.
     */
    getMerkleTree(): any[];
    /**
     * Calculates the merkleRoot from the transactions.
     * @returns {Buffer} - A buffer of the merkle root hash
     */
    getMerkleRoot(): Buffer;
    /**
     * Verifies that the transactions in the block match the header merkle root
     * @returns {Boolean} - If the merkle roots match
     */
    validMerkleRoot(): boolean;
    /**
     * @returns {Buffer} - The little endian hash buffer of the header
     */
    _getHash(): Buffer;
    id: any;
    hash: any;
    /**
     * @returns {string} - A string formatted for the console
     */
    inspect(): string;
}
declare namespace Block {
    let MAX_BLOCK_SIZE: number;
    /**
     * @param {*} - A Buffer, JSON string or Object
     * @returns {Object} - An object representing block data
     * @throws {TypeError} - If the argument was not recognized
     * @private
     */
    function _from(arg: any): any;
    /**
     * @param {Object} - A plain JavaScript object
     * @returns {Object} - An object representing block data
     * @private
     */
    function _fromObject(data: any): any;
    /**
     * @param {Object} - A plain JavaScript object
     * @returns {Block} - An instance of block
     */
    function fromObject(obj: any): Block;
    /**
     * @param {BufferReader} - Block data
     * @returns {Object} - An object representing the block data
     * @private
     */
    function _fromBufferReader(br: any): any;
    /**
     * @param {BufferReader} - A buffer reader of the block
     * @returns {Block} - An instance of block
     */
    function fromBufferReader(br: any): Block;
    /**
     * @param {Buffer} - A buffer of the block
     * @returns {Block} - An instance of block
     */
    function fromBuffer(buf: any): Block;
    /**
     * @param {string} - str - A hex encoded string of the block
     * @returns {Block} - A hex encoded string of the block
     */
    function fromString(str: any): Block;
    /**
     * @param {Binary} - Raw block binary data or buffer
     * @returns {Block} - An instance of block
     */
    function fromRawBlock(data: any): Block;
    namespace Values {
        let START_OF_BLOCK: number;
        let NULL_HASH: Buffer;
    }
}
import BufferWriter = require("../encoding/bufferwriter");
//# sourceMappingURL=block.d.ts.map