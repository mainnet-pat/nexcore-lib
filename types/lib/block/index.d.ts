declare const _exports: {
    (arg: any): import("./block");
    new (arg: any): import("./block");
    MAX_BLOCK_SIZE: number;
    _from(arg: any): any;
    _fromObject(data: any): any;
    fromObject(obj: any): import("./block");
    _fromBufferReader(br: any): any;
    fromBufferReader(br: any): import("./block");
    fromBuffer(buf: any): import("./block");
    fromString(str: any): import("./block");
    fromRawBlock(data: any): import("./block");
    Values: {
        START_OF_BLOCK: number;
        NULL_HASH: Buffer;
    };
    BlockHeader: typeof import("./blockheader");
    MerkleBlock: typeof import("./merkleblock");
};
export = _exports;
export const BlockHeader: typeof import("./blockheader");
export const MerkleBlock: typeof import("./merkleblock");
//# sourceMappingURL=index.d.ts.map