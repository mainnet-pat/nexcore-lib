'use strict';

var _ = require('lodash');
var $ = require('./util/preconditions');

var Script = require('./script');
var BN = require('./crypto/bn');
var Opcode = require('./opcode');
var BufferWriter = require('./encoding/bufferwriter');
var Hash = require('./crypto/hash');
var Address = require('./address');
var Message = require('./message');

var DEFAULT_OP_RETURN_GROUP_ID = 88888888;
var PARENT_GROUP_ID_SIZE = 32;

GroupToken.idFlags = {};
GroupToken.idFlags.NONE = 0;
GroupToken.idFlags.COVENANT = 1; // covenants/encumberances -- output script template must match input
GroupToken.idFlags.HOLDS_NEX = 1 << 1; // group inputs and outputs must balance NEX, token quantity MUST be 0
GroupToken.idFlags.GROUP_RESERVED_BITS = 0xFFFF & ~(GroupToken.idFlags.COVENANT | GroupToken.idFlags.HOLDS_NEX);
GroupToken.idFlags.DEFAULT = 0;

GroupToken.authFlags = {};
GroupToken.authFlags.AUTHORITY = 1n << 63n; // Is this a controller utxo (forces negative number in amount)
GroupToken.authFlags.MINT = 1n << 62n; // Can mint tokens
GroupToken.authFlags.MELT = 1n << 61n; // Can melt tokens,
GroupToken.authFlags.BATON = 1n << 60n; // Can create controller outputs
GroupToken.authFlags.RESCRIPT = 1n << 59n; // Can change the redeem script
GroupToken.authFlags.SUBGROUP = 1n << 58n; // Can create subgroups

GroupToken.authFlags.NONE = 0n;
GroupToken.authFlags.ACTIVE_FLAG_BITS = GroupToken.authFlags.AUTHORITY | GroupToken.authFlags.MINT | GroupToken.authFlags.MELT | GroupToken.authFlags.BATON | GroupToken.authFlags.RESCRIPT | GroupToken.authFlags.SUBGROUP;
GroupToken.authFlags.ALL_FLAG_BITS = 0xffffn << (64n - 16n);
GroupToken.authFlags.RESERVED_FLAG_BITS = GroupToken.authFlags.ACTIVE_FLAG_BITS & ~GroupToken.authFlags.ALL_FLAG_BITS;

/**
 * A GroupToken is merely a util containing methods for group tokenization for nexa blockchain.
 * @constructor
 */
function GroupToken() {}

/**
 * Build OP_RETURN output script for Token Description
 * 
 * @param {string} ticker
 * @param {string} name
 * @param {string} docUrl
 * @param {string} docHash
 * @param {number} decimals
 * @returns the output OP_RETURN script
 */
GroupToken.buildTokenDescScript = function(ticker, name, docUrl, docHash, decimals) {
  $.checkArgument(!_.isUndefined(ticker) && !_.isEmpty(ticker) && ticker.length < 9, 'Ticker must be lower than 9 chars');
  $.checkArgument(!_.isUndefined(name) && !_.isEmpty(name), 'Name is missing');

  var s = new Script().add(Opcode.OP_RETURN).add(new BN(DEFAULT_OP_RETURN_GROUP_ID).toScriptNumBuffer());
  s.add(Buffer.from(ticker)).add(Buffer.from(name));
  
  if (docUrl && docUrl.length > 0) {
    new URL(docUrl); // exception thrown if not valide
    $.checkArgument(!_.isUndefined(docHash) && !_.isEmpty(docHash), 'You must include document hash if you put document url');
    s.add(Buffer.from(docUrl)).add(Buffer.from(docHash, 'hex').reverse());
  } else {
    s.add(Opcode.OP_FALSE).add(Opcode.OP_FALSE);
  }

  if (_.isNumber(decimals)) {
    $.checkArgument(decimals >= 0 && decimals <= 18, 'decimals must be between 0 and 18');
    s.add(decimals <= 16 ? Opcode.smallInt(decimals) : new BN(decimals).toScriptNumBuffer());
  }

  return s;
}

/**
 * Calculate a group ID based on the provided inputs. Pass 'null' to opReturnScript if there is not
 * going to be an OP_RETURN output in the transaction.
 * 
 * @param {Buffer} outpointBuffer - The input outpoint hash buffer
 * @param {Buffer|null} opReturnScript - opReturn output script 
 * @param {bigint} authFlag - group control flags
 * @param {number} idFlag - group id flags
 * @returns Object with group id hash buffer and the nonce bigint
 */
GroupToken.findGroupId = function(outpointBuffer, opReturnScript, authFlag, idFlag = GroupToken.idFlags.DEFAULT) {
  var hash = '';
  var groupFlags = 0;
  var nonce = 0n;

  do {
    var writer = new BufferWriter();
    nonce += 1n;
    nonce = (nonce & ~GroupToken.authFlags.ALL_FLAG_BITS) | (authFlag);

    writer.writeReverse(outpointBuffer);
    if (opReturnScript != null) {
      writer.writeVarintNum(opReturnScript.length);
      writer.write(opReturnScript);
    }
    writer.writeUInt64LEBN(new BN(nonce.toString()));

    hash = Hash.sha256sha256(writer.toBuffer());
    groupFlags = (hash[30] << 8) | hash[31];
  } while (groupFlags != idFlag);

  return {hashBuffer: Buffer.from(hash), nonce: nonce};
}

/**
 * Translates a group and additional data into a subgroup identifier
 * 
 * @param {Address|string} group - the group/token address
 * @param {number|string} data - additional data
 * @returns the subgroup hashBuffer
 */
GroupToken.generateSubgroupId = function(group, data) {
  $.checkArgument(!_.isUndefined(group) && !_.isEmpty(group), 'group is missing');
  $.checkArgument(!_.isUndefined(data), 'data is missing');

  var groupAddress = new Address(group);
  $.checkArgument(groupAddress.isGroupedPayToPublicKeyTemplate(), 'No group specified');
  
  var buf = groupAddress.hashBuffer;

  if (_.isNumber(data)) {
    var bn = new BN(data);
    var bw = new BufferWriter().writeUInt64LEBN(bn).toBuffer();
    return Buffer.concat([buf, bw]);
  }

  return Buffer.concat([buf, Buffer.from(data)]);
}

/**
 * Get group amount buffer from BigInt to include in output script
 * 
 * @param {bigint} amount
 * @returns {Buffer}
 */
GroupToken.getAmountBuffer = function(amount) {
  var bn = new BN(amount.toString());
  var bw = new BufferWriter();
  if (amount < 0n) {
    bw.writeUInt64LEBN(bn)
  } else if (amount < 0x10000n) {
    bw.writeUInt16LE(bn.toNumber())
  } else if (amount < 0x100000000n) {
    bw.writeUInt32LE(bn.toNumber());
  } else {
    bw.writeUInt64LEBN(bn)
  }
  return bw.toBuffer();
}

/**
 * @param {bigint} authFlag the output group quantity/authority
 * @returns {bigint} the nonce
 */
GroupToken.getNonce = function(authFlag) {
  authFlag = BigInt.asUintN(64, authFlag);
  return authFlag & ~GroupToken.authFlags.ALL_FLAG_BITS;
}

/**
 * @param {Buffer} groupId the group id buffer
 * @param {number} groupIdFlag the group id flag
 * @returns {boolean} true if this group id has the flag
 */
GroupToken.hasIdFlag = function(groupId, groupIdFlag) {
  return groupId.length >= PARENT_GROUP_ID_SIZE ? (((groupId[30] << 8) | groupId[31]) & groupIdFlag) == groupIdFlag : false;
}

/**
 * @param {Buffer} groupId the group id buffer
 * @param {bigint} authFlag the output group quantity/authority
 * @param {bigint} groupIdFlag the group id flag
 * @returns {boolean} true if this is group creation data
 */
GroupToken.isGroupCreation = function(groupId, authFlag, groupIdFlag = GroupToken.idFlags.DEFAULT) {
  authFlag = BigInt.asUintN(64, authFlag);
  var hasNonce = GroupToken.getNonce(authFlag) != 0;
  var isAuth = GroupToken.isAuthority(authFlag);
  var hasFlag = GroupToken.hasIdFlag(groupId, groupIdFlag);
  return isAuth && hasNonce && hasFlag;
}

/**
 * @param {Buffer} groupId the group id buffer
 * @returns {boolean} true if this group id is subgroup
 */
GroupToken.isSubgroup = function(groupId) {
  return groupId.length > PARENT_GROUP_ID_SIZE;
}

/**
 * @param {bigint} authFlag the output group quantity/authority
 * @returns {boolean} true if this is authority flag
 */
GroupToken.isAuthority = function(authFlag) {
  return (authFlag & GroupToken.authFlags.AUTHORITY) == GroupToken.authFlags.AUTHORITY;
}

/**
 * @param {bigint} authFlag the output group quantity/authority
 * @returns {boolean} true if this flag allows minting.
 */
GroupToken.allowsMint = function(authFlag) {
  return (authFlag & (GroupToken.authFlags.AUTHORITY | GroupToken.authFlags.MINT)) == (GroupToken.authFlags.AUTHORITY | GroupToken.authFlags.MINT);
}

/**
 * @param {bigint} authFlag the output group quantity/authority
 * @returns {boolean} true if this flag allows melting.
 */
GroupToken.allowsMelt = function(authFlag) {
  return (authFlag & (GroupToken.authFlags.AUTHORITY | GroupToken.authFlags.MELT)) == (GroupToken.authFlags.AUTHORITY | GroupToken.authFlags.MELT);
}

/**
 * @param {bigint} authFlag the output group quantity/authority
 * @returns {boolean} true if this flag allows child controllers.
 */
GroupToken.allowsRenew = function(authFlag) {
  return (authFlag & (GroupToken.authFlags.AUTHORITY | GroupToken.authFlags.BATON)) == (GroupToken.authFlags.AUTHORITY | GroupToken.authFlags.BATON);
}

/**
 * @param {bigint} authFlag the output group quantity/authority
 * @returns {boolean} true if this flag allows rescripting.
 */
GroupToken.allowsRescript = function(authFlag) {
  return (authFlag & (GroupToken.authFlags.AUTHORITY | GroupToken.authFlags.RESCRIPT)) == (GroupToken.authFlags.AUTHORITY | GroupToken.authFlags.RESCRIPT);
}

/**
 * @param {bigint} authFlag the output group quantity/authority
 * @returns {boolean} true if this flag allows subgroups
 */
GroupToken.allowsSubgroup = function(authFlag) {
  return (authFlag & (GroupToken.authFlags.AUTHORITY | GroupToken.authFlags.SUBGROUP)) == (GroupToken.authFlags.AUTHORITY | GroupToken.authFlags.SUBGROUP);
}

/**
 * Verify token description document json signature
 * 
 * @param {string} jsonDoc the json TDD as string
 * @param {Address|string} address nexa address that signed the doc
 * @param {string} signature the signature. optional - if empty, extract from jsonDoc
 * @returns {boolean}
 */
GroupToken.verifyJsonDoc = function(jsonDoc, address, signature) {
  $.checkArgument(!_.isUndefined(jsonDoc) && !_.isEmpty(jsonDoc), 'jsonDoc is missing');
  $.checkArgument(!_.isUndefined(address) && !_.isEmpty(address), 'group is missing');
  
  var json = jsonDoc.substring(jsonDoc.indexOf('{'), jsonDoc.lastIndexOf('}') + 1);
  if (_.isUndefined(signature) || _.isEmpty(signature)) {
    signature = JSON.parse(jsonDoc)[1];
  }
  var msg = new Message(json);
  return msg.verify(address, signature);
}

/**
 * Sign token description document json
 * 
 * @param {string} jsonDoc the json TDD as string 
 * @param {PrivateKey} privKey private key to sign on the doc
 * @returns {string}
 */
GroupToken.signJsonDoc = function(jsonDoc, privKey) {
  var json = jsonDoc.substring(jsonDoc.indexOf('{'), jsonDoc.lastIndexOf('}') + 1); // trimming
  var msg = new Message(json);
  return msg.sign(privKey);
}

/**
 * @namespace GroupToken
 */
module.exports = GroupToken;