'use strict';

var _ = require('lodash');
var nexaaddr = require('nexaaddrjs');
var $ = require('./util/preconditions');
var Networks = require('./networks');
var Hash = require('./crypto/hash');
var JSUtil = require('./util/js');
var PublicKey = require('./publickey');
var Opcode = require('./opcode');

/**
 * Instantiate an address from an address String or Buffer, a public key or script hash Buffer,
 * or an instance of {@link PublicKey} or {@link Script}.
 *
 * This is an immutable class, and if the first parameter provided to this constructor is an
 * `Address` instance, the same argument will be returned.
 *
 * An address has two key properties: `network` and `type`. The type is either
 * `Address.PayToPublicKeyHash` (value is the `'pubkeyhash'` string)
 * or `Address.PayToScriptHash` (the string `'scripthash'`). The network is an instance of {@link Network}.
 * You can quickly check whether an address is of a given kind by using the methods
 * `isPayToPublicKeyHash` and `isPayToScriptHash`
 *
 * @example
 * ```javascript
 * // validate that an input field is valid
 * var error = Address.getValidationError(input, 'testnet');
 * if (!error) {
 *   var address = Address(input, 'testnet');
 * } else {
 *   // invalid network or checksum (typo?)
 *   var message = error.messsage;
 * }
 *
 * // get an address from a public key
 * var address = Address(publicKey, 'testnet').toString();
 * ```
 *
 * @param {*} data - The encoded data in various formats
 * @param {Networks.Network} network - The network: 'livenet' or 'testnet'
 * @param {string=} type - The type of address: 'script' or 'pubkey'
 * @returns {Address} A new valid and frozen instance of an Address
 * @constructor
 */
function Address(data, network, type) {
  /* jshint maxcomplexity: 12 */
  /* jshint maxstatements: 20 */

  if (!(this instanceof Address)) {
    return new Address(data, network, type);
  }

  if (_.isArray(data) && _.isNumber(network)) {
    return Address.createMultisig(data, network, type);
  }

  if (_.isString(data)) {
    return Address.decodeNexaAddress(data);
  }

  if (data instanceof Address) {
    // Immutable instance
    return data;
  }

  $.checkArgument(data, 'First argument is required, please include address data.', 'guide/address.html');

  if (network && !Networks.get(network)) {
    throw new TypeError('Second argument must be "livenet", "testnet", or "regtest".');
  }

  if (type && (type !== Address.PayToScriptTemplate && type !== Address.GroupedPayToPublicKeyTemplate && type !== Address.PayToPublicKeyHash)) {
    throw new TypeError('Third argument must be "p2pkt", "p2pkh", "gp2pkt" or "p2st".');
  }

  JSUtil.defineImmutable(this, {
    hashBuffer: data,
    network: network || Networks.defaultNetwork,
    type: type || Address.PayToScriptTemplate
  });

  return this;
}

/** @static */
Address.PayToPublicKeyHash = 'P2PKH';
/** @static */
Address.PayToScriptHash = 'SCRIPT';
/** @static */
Address.PayToScriptTemplate = 'TEMPLATE';
/** @static */
Address.GroupedPayToPublicKeyTemplate = 'GROUP';

/**
 * @param {Buffer} hash - An instance of a hash Buffer
 * @returns {Object} An object with keys: hashBuffer
 * @private
 */
Address._transformHash = function(hash) {
  var info = {};
  if (!(hash instanceof Buffer) && !(hash instanceof Uint8Array)) {
    throw new TypeError('Address supplied is not a buffer.');
  }
  if (hash.length !== 20) {
    throw new TypeError('Address hashbuffers must be exactly 20 bytes.');
  }
  info.hashBuffer = hash;
  return info;
};

/**
 * @param {Buffer|Opcode|number} templateHash - An instance of a template hash Buffer
 * @param {Buffer|Opcode|number} constraintHash - An instance of a constraint hash Buffer
 * @param {Array} visibleArgs - An array of push-only args
 * @returns {Object} An object with keys: hashBuffer
 * @private
 */
Address._transformTemplate = function(templateHash, constraintHash, visibleArgs) {
  var info = {};
  if (templateHash != Opcode.OP_1 && !(templateHash instanceof Buffer) && !(templateHash instanceof Uint8Array)) {
    throw new TypeError('templateHash supplied is not a buffer or well-known OP_1.');
  }
  if (templateHash != Opcode.OP_1 && templateHash.length !== 20) {
    throw new TypeError('templateHash buffer must be 20 bytes or well-known OP_1.');
  }
  if (constraintHash != Opcode.OP_FALSE && !(constraintHash instanceof Buffer) && !(constraintHash instanceof Uint8Array)) {
    throw new TypeError('constraintHash supplied is not a buffer or OP_FALSE.');
  }
  if (constraintHash != Opcode.OP_FALSE && constraintHash.length !== 20) {
    throw new TypeError('constraintHash buffer must be 20 bytes or OP_FALSE.');
  }
  var scriptTemplate = Script.empty().add(Opcode.OP_FALSE).add(templateHash).add(constraintHash);
  if (visibleArgs) {
    visibleArgs.forEach(arg => scriptTemplate.add(arg));
  }
  info.hashBuffer = Script.empty().add(scriptTemplate.toBuffer()).toBuffer();
  return info;
};

/**
 * Deserializes an address serialized through `Address#toObject()`
 * @param {Object} data
 * @param {string} data.hash - the hash that this address encodes
 * @param {string} data.type - either 'pubkeyhash' or 'scripthash'
 * @param {Network=} data.network - the name of the network associated
 * @return {Address}
 */
Address._transformObject = function(data) {
  $.checkArgument(data.hash || data.hashBuffer, 'Must provide a `hash` or `hashBuffer` property');
  $.checkArgument(data.type, 'Must provide a `type` property');
  return {
    hashBuffer: data.hash ? Buffer.from(data.hash, 'hex') : data.hashBuffer,
    network: Networks.get(data.network) || Networks.defaultNetwork,
    type: data.type
  };
};

/**
 * Creates a P2SH address from a set of public keys and a threshold.
 *
 * The addresses will be sorted lexicographically, as that is the trend in bitcoin.
 * To create an address from unsorted public keys, use the {@link Script#buildMultisigOut}
 * interface.
 *
 * @param {Array} publicKeys - a set of public keys to create an address
 * @param {number} threshold - the number of signatures needed to release the funds
 * @param {String|Network} network - either a Network instance, 'livenet', or 'testnet'
 * @return {Address}
 */
Address.createMultisig = function(publicKeys, threshold, network) {
  network = network || publicKeys[0].network || Networks.defaultNetwork;
  return Address.payingTo(Script.buildMultisigOut(publicKeys, threshold), network);
};

/**
 * Creates a P2SH Zero-Confirmation Escrow (ZCE) address from a set of input public keys and a reclaim public key.
 *
 * @param {Array} inputPublicKeys - the set of public keys needed to sign all inputs in a ZCE transaction
 * @param {PublicKey} reclaimPublicKey - the public key required to reclaim the escrow
 * @param {String|Network} network - either a Network instance, 'livenet', or 'testnet'
 * @return {Address}
 */
 Address.createEscrow = function(inputPublicKeys, reclaimPublicKey, network) {
  const zceRedeemScript = Script.buildEscrowOut(inputPublicKeys, reclaimPublicKey);
  network = network || reclaimPublicKey.network || Networks.defaultNetwork;
  return Address.payingTo(zceRedeemScript, network);
};

/**
 * Instantiate an address from a PublicKey instance
 *
 * @param {PublicKey} data
 * @param {String|Network} network - either a Network instance, 'livenet', or 'testnet'
 * @returns {Address} A new valid and frozen instance of an Address
 */
Address.fromPublicKey = function(data, network, type) {
  var info = Address._transformPublicKey(data, type || Address.PayToScriptTemplate);
  network = network || Networks.defaultNetwork;
  return new Address(info.hashBuffer, network, info.type);
};

/**
 * Internal function to transform a {@link PublicKey}
 *
 * @param {PublicKey} pubkey - An instance of PublicKey
 * @returns {Object} An object with keys: hashBuffer, type
 * @private
 */
Address._transformPublicKey = function(pubkey, type) {
  var info = {};
  if (!(pubkey instanceof PublicKey)) {
    throw new TypeError('Address must be an instance of PublicKey.');
  }
  if (type === Address.PayToPublicKeyHash) {
    info.hashBuffer = Hash.sha256ripemd160(pubkey.toBuffer());
  } else if (type === Address.PayToScriptTemplate) {
    var constraint = new Script().add(pubkey.toBuffer());
    var constraintHash = Hash.sha256ripemd160(constraint.toBuffer());
    info.hashBuffer = Address._transformTemplate(Opcode.OP_1, constraintHash).hashBuffer;
  }
  info.type = type;
  return info;
};

/**
 * WARNING: This method is deprecated - P2SH disabled on nexa mainnet.
 * Instantiate an address from a ripemd160 script hash
 *
 * @deprecated
 * @param {Buffer} hash - An instance of buffer of the hash
 * @param {String|Network} network - either a Network instance, 'livenet', or 'testnet'
 * @returns {Address} A new valid and frozen instance of an Address
 */
Address.fromScriptHash = function(hash, network) {
  $.checkArgument(hash, 'hash parameter is required');
  var info = Address._transformHash(hash);
  return new Address(info.hashBuffer, network, Address.PayToScriptHash);
};

/**
 * Instantiate an address from a non grouped script template
 *
 * @param {Buffer|Opcode|number} templateHash - An instance the template hash
 * @param {Buffer|Opcode|number} constraintHash - An instance the constraint hash
 * @param {Array} visibleArgs - An array of push-only args
 * @param {Networks.Network} network - either a Network instance, 'livenet', or 'testnet'
 * @returns {Address} A new valid and frozen instance of an Address
 */
Address.fromScriptTemplate = function(templateHash, constraintHash, visibleArgs = undefined, network = undefined) {
  $.checkArgument(templateHash, 'templateHash parameter is required');
  $.checkArgument(constraintHash || constraintHash === Opcode.OP_FALSE, 'constraintHash parameter is required');
  var info = Address._transformTemplate(templateHash, constraintHash, visibleArgs);
  return new Address(info.hashBuffer, network, Address.PayToScriptTemplate);
};

/**
 * WARNING: This method is deprecated - P2SH disabled on nexa mainnet.
 * 
 * Builds a p2sh address paying to script. This will hash the script and
 * use that to create the address.
 * If you want to extract an address associated with a script instead,
 * see {{Address#fromScript}}
 *
 * @deprecated
 * @param {Script} script - An instance of Script
 * @param {String|Network} network - either a Network instance, 'livenet', or 'testnet'
 * @returns {Address} A new valid and frozen instance of an Address
 */
Address.payingTo = function(script, network) {
  $.checkArgument(script, 'script is required');
  $.checkArgument(script instanceof Script, 'script must be instance of Script');

  return Address.fromScriptHash(Hash.sha256ripemd160(script.toBuffer()), network);
};

/**
 * Extract address from a Script. The script must be of one
 * of the following types: p2pkh input, p2pkh output, p2sh input
 * or p2sh output.
 * This will analyze the script and extract address information from it.
 * If you want to transform any script to a p2sh Address paying
 * to that script's hash instead, use {{Address#payingTo}}
 *
 * @param {Script} script - An instance of Script
 * @param {String|Network} network - either a Network instance, 'livenet', or 'testnet'
 * @returns {Address} A new valid and frozen instance of an Address

Address.fromScript = function(script, network) {
  $.checkArgument(script instanceof Script, 'script must be a Script instance');
  var info = Address._transformScript(script, network);
  return new Address(info.hashBuffer, network, info.type);
};
 */

/**
 * Instantiate an address from an address string
 *
 * @param {string} str - An string of the bitcoin address
 * @param {Networks.Network} network - either a Network instance, 'livenet', or 'testnet'
 * @param {string=} type - The type of address: 'script' or 'pubkey'
 * @returns {Address} A new valid and frozen instance of an Address
 */
Address.fromString = function(str, network, type) {
  var info = Address._transformString(str, network, type);
  return new Address(info.hashBuffer, info.network, info.type);
};

/**
 * Internal function to transform a nexa address string
 *
 * @param {string} data
 * @param {String|Network=} network - either a Network instance, 'livenet', or 'testnet'
 * @param {string=} type - The type: 'pubkeyhash' or 'scripthash'
 * @returns {Object} An object with keys: hashBuffer, network and type
 * @private
 */
Address._transformString = function(data, network, type) {
  if (typeof(data) !== 'string') {
    throw new TypeError('data parameter supplied is not a string.');
  }
  if (data.length < 34){
    throw new Error('Invalid Address string provided');
  }

  if(data.length > 100) {
    throw new TypeError('address string is too long');
  }

  var dec = nexaaddr.decode(data);
  var info = {};
  info.hashBuffer = Buffer.from(dec.hash);
  info.network = network || Networks.defaultNetwork;
  info.type = type || Address.PayToScriptTemplate

  return info;
};

/**
 * Instantiate an address from an Object
 *
 * @param {string} json - An JSON string or Object with keys: hash, network and type
 * @returns {Address} A new valid instance of an Address
 */
Address.fromObject = function fromObject(obj) {
  $.checkState(
    JSUtil.isHexa(obj.hash),
    'Unexpected hash property, "' + obj.hash + '", expected to be hex.'
  );
  var hashBuffer = Buffer.from(obj.hash, 'hex');
  return new Address(hashBuffer, obj.network, obj.type);
};

/**
 * Will return a validation error if exists
 *
 * @example
 * ```javascript
 * // a network mismatch error
 * var error = Address.getValidationError('15vkcKf7gB23wLAnZLmbVuMiiVDc1Nm4a2', 'testnet');
 * ```
 *
 * @param {string|Buffer|Uint8Array} data - The encoded data
 * @param {Networks.Network} network - either a Network instance, 'livenet', or 'testnet'
 * @param {string} type - The type of address: 'TEMPLATE' or 'GROUP' or 'P2PKH'
 * @returns {null|Error} The corresponding error message
 */
Address.getValidationError = function(data, network, type) {
  var error;
  try {
    if (_.isString(data)) {
      let addr = Address.decodeNexaAddress(data);
      data = addr.hashBuffer;
      network = addr.network;
      type = addr.type;
    }
    /* jshint nonew: false */
    nexaaddr.encode(network ? network.prefix : Networks.defaultNetwork.prefix, type || this.PayToScriptTemplate, data);
    new Address(data, network, type);
  } catch (e) {
    error = e;
  }
  return error;
};

/**
 * Will return a boolean if an address is valid
 *
 * @example
 * ```javascript
 * assert(Address.isValid('15vkcKf7gB23wLAnZLmbVuMiiVDc1Nm4a2', 'livenet'));
 * ```
 *
 * @param {string|Buffer|Uint8Array} data - The encoded data
 * @param {Networks.Network} network - either a Network instance, 'livenet', or 'testnet'
 * @param {string} type - The type of address: 'TEMPLATE' or 'GROUP' or 'P2PKH'
 * @returns {boolean} The corresponding error message
 */
Address.isValid = function(data, network = undefined, type = undefined) {
  return !Address.getValidationError(data, network, type);
};

/**
 * 
 * @param {string} address 
 * @returns {Address} A new valid and frozen instance of an Address
 */
Address.decodeNexaAddress = function(address) {
  var parts = nexaaddr.decode(address);
  var network = parts.prefix.includes('test') ? Networks.testnet : Networks.livenet;
  return new Address(Buffer.from(parts.hash), network, parts.type);
}

/**
 * Returns true if an address is of pay to public key hash type
 * @return boolean
 */
Address.prototype.isPayToPublicKeyHash = function() {
  return this.type === Address.PayToPublicKeyHash;
};

/**
 * Returns true if an address is of pay to script hash type
 * @return boolean
 */
Address.prototype.isPayToScriptHash = function() {
  return this.type === Address.PayToScriptHash;
};

/**
 * Returns true if an address is of pay to script template type
 * @return boolean
 */
Address.prototype.isPayToScriptTemplate = function() {
  return this.type === Address.PayToScriptTemplate;
};

/**
 * Returns true if an address is of pay to grouped template type
 * @return boolean
 */
Address.prototype.isGroupedPayToPublicKeyTemplate = function() {
  return this.type === Address.GroupedPayToPublicKeyTemplate;
};


/**
 * Will return a buffer representation of the address
 *
 * @returns {Buffer} Bitcoin address buffer

Address.prototype.toBuffer = function() {
  var version = Buffer.from([this.network[this.type]]);
  var buf = Buffer.concat([version, this.hashBuffer]);
  return buf;
};
 */

/**
 * @returns {Object} A plain object with the address information
 */
Address.prototype.toObject = Address.prototype.toJSON = function toObject() {
  return {
    hash: this.hashBuffer.toString('hex'),
    type: this.type,
    network: this.network.toString()
  };
};

/**
 * Will return a string formatted for the console
 *
 * @returns {string} Bitcoin address
 */
Address.prototype.inspect = function() {
  return '<Address: ' + this.toString() + ', type: ' + this.type + ', network: ' + this.network + '>';
};

/**
 * Will return a cashaddr representation of the address. Always return lower case
 * Can be converted by the caller to uppercase is needed (still valid).
 *
 * @returns {string} Nexa address
 */
Address.prototype.toNexaAddress = Address.prototype.toString = function toNexaAddress() {
  return nexaaddr.encode(this.network.prefix, this.type, this.hashBuffer);
};

/**
 * Will return the transaction output type by the address type
 * 
 * @param {string} address 
 * @returns {number} 1 - Template, 0 - otherwise
 */
Address.getOutputType = function(address) {
  var addr = nexaaddr.decode(address);
  if (addr.type === this.PayToScriptTemplate || addr.type == this.GroupedPayToPublicKeyTemplate) {
    return 1;
  } 
  return 0;
}

module.exports = Address;

var Script = require('./script');
