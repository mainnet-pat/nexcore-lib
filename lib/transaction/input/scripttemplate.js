'use strict';

var inherits = require('inherits');

var $ = require('../../util/preconditions');

var Hash = require('../../crypto/hash');
var Input = require('./input');
var Output = require('../output');
var Sighash = require('../sighash');
var Script = require('../../script');
var Signature = require('../../crypto/signature');
var TransactionSignature = require('../signature');
var Opcode = require('../../opcode');
var JSUtil = require('../../util/js');

/**
 * Represents a special kind of input of not well-known PayToScriptTemplate kind.
 * @constructor
 */
function ScriptTemplateInput(input, templateScript, constraintScript, visibleArgs, pulicKey) {
  Input.apply(this, arguments);
  this.visibleArgs = visibleArgs || input.visibleArgs;
  this.publicKey = pulicKey || input.publicKey;

  this.templateScript = templateScript || input.templateScript;
  if (JSUtil.isHexa(this.templateScript)) {
    this.templateScript = Script.fromHex(this.templateScript);
  }

  this.constraintScript = constraintScript || input.constraintScript;
  if (this.constraintScript != 0 && JSUtil.isHexa(this.constraintScript)) {
    this.constraintScript = Script.fromHex(this.constraintScript);
  }

  var templateHash = Hash.sha256ripemd160(this.templateScript.toBuffer());
  var constraintHash = this.constraintScript === Opcode.OP_FALSE ? Opcode.OP_FALSE : Hash.sha256ripemd160(this.constraintScript.toBuffer());
  var scriptTemplate = Script.empty().add(Opcode.OP_FALSE).add(templateHash).add(constraintHash);
  if (this.visibleArgs) {
    if (JSUtil.isHexa(this.visibleArgs)) {
      scriptTemplate.add(Script.fromHex(this.visibleArgs));
    } else {
      this.visibleArgs.forEach(arg => scriptTemplate.add(arg));
    }
  }
  $.checkState(scriptTemplate.equals(this.output.script), "Provided scripts don't hash to the provided output");
}
inherits(ScriptTemplateInput, Input);

ScriptTemplateInput.prototype.toObject = function() {
  var obj = Input.prototype.toObject.apply(this, arguments);

  obj.templateScript = this.templateScript.toHex();
  obj.templateScriptString = this.templateScript.toString();

  if (this.constraintScript === Opcode.OP_FALSE) {
    obj.constraintScript = 0;
  } else {
    obj.constraintScript = this.constraintScript.toHex();
    obj.constraintScriptString = this.constraintScript.toString();
  }

  obj.visibleArgs = this.visibleArgs;
  if (this.visibleArgs && !JSUtil.isHexa(this.visibleArgs)) {
    var s = Script.empty();
    this.visibleArgs.forEach(arg => s.add(arg));
    obj.visibleArgs = s.toHex();
  }
  obj.publicKey = this.publicKey.toString();
  return obj;
};

/* jshint maxparams: 5 */
/**
 * @param {Transaction} transaction - the transaction to be signed
 * @param {PrivateKey} privateKey - the private key with which to sign the transaction
 * @param {number} index - the index of the input in the transaction input vector
 * @param {number=} sigtype - the type of signature, defaults to Signature.SIGHASH_ALL
 * @param {Buffer=} hashData - the precalculated hash of the public key associated with the privateKey provided
 * @param {String} signingMethod - the signing method used to sign tx "ecdsa" or "schnorr"
 * @return {Array} of objects that can be
 */
ScriptTemplateInput.prototype.getSignatures = function(transaction, privateKey, index, sigtype, hashData, signingMethod) {
  $.checkState(this.output instanceof Output);
  if (this.publicKey.toString() !== privateKey.publicKey.toString()) {
    return [];
  }

  return [new TransactionSignature({
    publicKey: privateKey.publicKey,
    prevTxId: this.prevTxId,
    outputIndex: this.outputIndex,
    inputIndex: index,
    signature: Sighash.sign(transaction, privateKey, sigtype, index, this.templateScript),
    sigtype: sigtype
  })];
};
/* jshint maxparams: 3 */

/**
 * Add the provided signature
 *
 * @param {Object} signature
 * @param {PublicKey} signature.publicKey
 * @param {Signature} signature.signature
 * @param {number=} signature.sigtype
 * @param {String} signingMethod only "schnorr" allowed
 * @return {ScriptTemplateInput} this, for chaining
 */
ScriptTemplateInput.prototype.addSignature = function(transaction, signature, signingMethod) {

  $.checkState(this.isValidSignature(transaction, signature, signingMethod), 'Signature is invalid');

  this.setScript(Script.buildScriptTemplateIn(
    this.templateScript,
    this.constraintScript,
    signature.signature.toDER(),
    signature.sigtype
  ));
  return this;
};

ScriptTemplateInput.prototype.isValidSignature = function(transaction, signature, signingMethod) {
  signature.signature.nhashtype = signature.sigtype;
  return Sighash.verify(
    transaction,
    signature.signature,
    signature.publicKey,
    signature.inputIndex,
    this.templateScript,
    this.output.satoshisBN,
    undefined,
    signingMethod
  );
};

/**
 * Clear the input's signature
 * @return {ScriptTemplateInput} this, for chaining
 */
ScriptTemplateInput.prototype.clearSignatures = function() {
  this.setScript(Script.empty());
  return this;
};

/**
 * Query whether the input is signed
 * @return {boolean}
 */
ScriptTemplateInput.prototype.isFullySigned = function() {
  return this.script.isScriptTemplateIn();
};

ScriptTemplateInput.prototype._estimateSize = function() {
  // type + outpoint + scriptlen + script + sequence + amount
  var scriptSize = this._estimateScriptSize();
  return 1 + 32 + (scriptSize < 253 ? 1 : 3) + scriptSize + 4 + 8;
};

ScriptTemplateInput.prototype._estimateScriptSize = function() {
  // for now we calculate by template size, constraint size if not op_false, and satisfier size as sigsize (1 + 64)
  var s = Script.empty().add(this.templateScript.toBuffer());
  if (this.constraintScript instanceof Script) {
    s.add(this.constraintScript.toBuffer());
  }
  return s.toBuffer().length + 1 + 64;
};

module.exports = ScriptTemplateInput;
